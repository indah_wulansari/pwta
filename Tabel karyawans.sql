/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 10.4.17-MariaDB : Database - pwta
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`pwta` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `pwta`;

/*Table structure for table `karyawans` */

DROP TABLE IF EXISTS `karyawans`;

CREATE TABLE `karyawans` (
  `karnik` bigint(20) unsigned NOT NULL,
  `karnama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `karjk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kartl` date NOT NULL,
  `karal` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kartelp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bagid` int(11) NOT NULL,
  `cabkode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kartgljoin` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`karnik`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `karyawans` */

insert  into `karyawans`(`karnik`,`karnama`,`email`,`karjk`,`kartl`,`karal`,`kartelp`,`bagid`,`cabkode`,`kartgljoin`,`created_at`,`updated_at`) values (3505021406980001,'Karisma Piawai Sampurna','piawaysampurna123@gmail.com','Pria','1998-06-16','Jl. Raya Tegalan, Desa Tegalan, Kecamatan Kandat, Kabupaten Kediri','085232049663',5,NULL,'2017-03-23','2021-06-13 20:23:45','2021-06-13 20:23:45'),(3506052501980005,'Andri Rio Pramudian','andririo735@gmail.com','Pria','1998-04-25','Dusun Bangsal, Desa Selosari, Kecamatan Kandat, Kabupaten Kediri','089765432567',1,NULL,'2017-01-01','2021-02-08 14:36:50','2021-05-24 18:38:46'),(3506052505970006,'Bambang Sugianto','Bambanganto252gmail.com','Pria','1997-05-25','Dusun Kartosari, Desa Kandat, Kecamatan Kandat, Kabupaten Kediri','082271890764',8,'CAB-001','2017-06-24','2021-05-24 18:04:39','2021-05-28 21:47:17'),(3506052811010001,'Muchtar Al Husain','smar8823@gmail.com','Pria','2001-11-28','Dusun Dungpung, Desa Cendono, Kecamatan Kandat, Kabupaten Kediri','085746744634',8,'CAB-002','2017-01-24','2021-05-24 18:07:30','2021-05-24 18:07:30'),(3506055402000005,'Cindy Widyan Valentina','cindytina014@gmail.com','Wanita','2001-02-14','Dusun Kartosari, Desa Kandat, Kecamatan Kandat, Kabupaten Kediri','081345620987',8,'CAB-001','2017-05-24','2021-05-24 18:01:40','2021-05-24 18:01:40'),(3506262909960002,'Danang Ragil Pamungkas','danangpmks96@gmail.com','Pria','1996-09-25','Dusun Lamong, Desa Lamong, Kecamatan Badas, Kabupaten Kediri','081234634934',8,'CAB-002','2017-02-12','2021-05-28 21:44:54','2021-05-28 21:44:54'),(3506265806910002,'Bella Lizawana','bellaliz91@gmail.com','Wanita','1991-06-18','Dusun Lamong, Desa Lamong, Kecamatan Badas, Kabupaten Kediri','089575859173',8,'CAB-001','2020-05-28','2021-05-28 21:51:04','2021-05-28 21:51:04');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
