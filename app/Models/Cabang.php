<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Karyawan;

class Cabang extends Model
{
    use HasFactory;
    protected $table = 'cabangs';
    protected $primaryKey = 'cabkode';
    public $incrementing = false;
    protected $fillable = [
        'cabkode',
        'cabnama'
    ];
}
