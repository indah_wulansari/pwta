<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bonpot extends Model
{
    use HasFactory;

    protected $table = 'bonpots';
    protected $primaryKey='bpid';
    protected $fillable = [
        'bonus', 'potongan'
    ];
}
