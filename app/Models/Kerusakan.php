<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Cabang;


class Kerusakan extends Model
{
    use HasFactory;
    protected $table = 'kerusakans';
    protected $primaryKey = 'rusakid';
    public $incrementing = false;
    protected $fillable = [
        'rusakid',
        'cabkode',
        'rusaknama',
        'rusaktgl',
        'rusakjml',
        'keterangan',
        'hargapenggantian'
    ];

    public function cabang()
    {
        return $this->belongsTo(Cabang::class, 'cabkode');
    }
}
