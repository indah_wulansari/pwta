<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Absensi extends Model
{
    use HasFactory;
    protected $table = 'absensis';
    protected $primaryKey = 'abid';
    // public $incrementing = false;
    protected $fillable = [
        'abid',
        'karnik',
        'karnama',
        'abhar',
        'abmas',
        'abkel' 
    ];

    public function karyawan()
    {
        return $this->belongsTo(Karyawan::class, 'karnik');
    }
}
