<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Bagian;
use App\Models\Cabang;

class Karyawan extends Model
{
    use HasFactory;
    protected $table = 'karyawans';
    protected $primaryKey = 'karnik';
    public $incrementing = false;
    protected $fillable = [
        'karnik',
        'karnama',
        'email',
        'karjk',
        'kartl',
        'karal',
        'kartelp',
        'bagid',
        'cabkode',  
        'kartgljoin'   
    ];

    public function bagian()
    {
        return $this->belongsTo(Bagian::class, 'bagid');
    }

    public function cabang()
    {
        return $this->belongsTo(Cabang::class, 'cabkode');
    }

    public function absensi()
    {
        return $this->hasMany(Absensi::class, 'karnik');
    }
}
