<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Karyawan;

class Bagian extends Model
{
    use HasFactory;

    protected $table = 'bagians';
    protected $primaryKey='bagid';
    protected $fillable = [
        'bagnama', 'bagpok'
    ];

}
