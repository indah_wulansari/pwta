<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\SupplyBarang;

class MenuMakanan extends Model
{
    use HasFactory;
    protected $table = 'menu_makanans';
    protected $primaryKey = 'menukd';
    public $incrementing = false;
    protected $fillable = [
        'menukd',
        'bkode',
        'stok',
        'hargajual'
    ];

    public function spbarang()
    {
        return $this->belongsTo(SupplyBarang::class, 'bkode');
    }
}
