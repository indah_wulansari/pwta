<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DataTables;
use App\Models\Absensi;
use App\Models\Bonpot;
use Session;
use App\Models\Bagian;


class GajiController extends Controller
{
 
    public function index(Request $request)
    {
        return view('admin.gaji');        
    }

    public function dataTable_gaji(Request $request)
    {
        $user 		= Session::get("data_user");
        $level = $user['level'];
        $karnik = $user['karnik'];
        $pot = Bonpot::select('potongan')->first();
        $bonus = Bonpot::select('bonus')->first();
        
        if ($level == 'pemilik' || $level == 'admin') {
            $query = DB::select("SELECT ab.karnik,kr.karnama,bg.bagnama, ab.abhar, ab.jumlah_hari, ab.jumlah_masuk, ab.potongan, IF(jumlah_masuk=jumlah_hari,COALESCE(bp.`bonus`, '0'),'0') AS bonus,bagpok, (ROUND(bagpok/jumlah_hari*jumlah_masuk) + IF(jumlah_masuk=jumlah_hari,COALESCE(bp.`bonus`, '0'),'0') - ab.potongan) AS total FROM
            (
            SELECT a.karnik,COUNT(a.abhar) AS jumlah_masuk,DAY(LAST_DAY(a.abhar)) AS jumlah_hari, SUM(potongan) AS potongan, a.abhar FROM 
            (SELECT absensis.*, IF(abmas>'16:00:00', bp.`potongan`,0) AS potongan FROM absensis LEFT JOIN bonpots AS bp ON bp.`tahun` = YEAR(absensis.`abhar`) AND bp.`bulan` = MONTH(absensis.`abhar`))a
            GROUP BY karnik, MONTH(abhar)
            )ab
            LEFT JOIN karyawans AS kr ON kr.karnik = ab.karnik 
            LEFT JOIN bagians AS bg ON bg.bagid = kr.bagid
            LEFT JOIN bonpots AS bp ON bp.`tahun` = YEAR(ab.abhar) AND bp.`bulan` = MONTH(ab.abhar)");
        } else {
            $query = DB::select("SELECT ab.karnik,kr.karnama,bg.bagnama, ab.abhar, ab.jumlah_hari, ab.jumlah_masuk, ab.potongan, IF(jumlah_masuk=jumlah_hari,COALESCE(bp.`bonus`, '0'),'0') AS bonus,bagpok, (ROUND(bagpok/jumlah_hari*jumlah_masuk) + IF(jumlah_masuk=jumlah_hari,COALESCE(bp.`bonus`, '0'),'0') - ab.potongan) AS total FROM
            (
            SELECT a.karnik,COUNT(a.abhar) AS jumlah_masuk,DAY(LAST_DAY(a.abhar)) AS jumlah_hari, SUM(potongan) AS potongan, a.abhar FROM 
            (SELECT absensis.*, IF(abmas>'16:00:00', bp.`potongan`,0) AS potongan FROM absensis LEFT JOIN bonpots AS bp ON bp.`tahun` = YEAR(absensis.`abhar`) AND bp.`bulan` = MONTH(absensis.`abhar`))a
            GROUP BY karnik, MONTH(abhar)
            )ab
            LEFT JOIN karyawans AS kr ON kr.karnik = ".$karnik." 
            LEFT JOIN bagians AS bg ON bg.bagid = kr.bagid
            LEFT JOIN bonpots AS bp ON bp.`tahun` = YEAR(ab.abhar) AND bp.`bulan` = MONTH(ab.abhar)");
        }
        $data = array();

        $no=1;
        foreach ($query as $key => $value) {
            $data[$key]['no']=$no++;
            $data[$key]['karnik']=$value->karnik;
            $data[$key]['karnama']=$value->karnama;
            $data[$key]['bagnama']=$value->bagnama;
            $data[$key]['abhar']=$value->abhar;
            $data[$key]['jumlah_hari']=$value->jumlah_hari;
            $data[$key]['jumlah_masuk']=$value->jumlah_masuk;
            $data[$key]['potongan']=$value->potongan;
            $data[$key]['bonus']=$value->bonus;
            $data[$key]['bagpok']=$value->bagpok;
            $data[$key]['total']=$value->total;
        }   
        
            return DataTables::of($data)
                ->addColumn('action', function($datas){
                    $buttonEdit ='<a class="btn btn-sm btn-warning editDetail mr-2"
                    data-abhar="'.$datas['abhar'].'" 
                    data-karnik="'.$datas['karnik'].'"
                    data-jumlah_hari="'.$datas['jumlah_hari'].'"
                    data-jumlah_masuk="'.$datas['jumlah_masuk'].'"
                    data-karnama="'.$datas['karnama'].'"
                    href="javascript:void(0)"><i class="far fa-edit"></i>&nbsp;detail</a>';
                               
                    return $buttonEdit;
                })
                ->rawColumns(['action'])
                ->make(true);
    }

    public function getKelalaian(Request $request)
    {
        $date = $request->tanggal;
        $karnik = $request->karnik;
        $tanggal = strtotime($date);
        $tahun = date("Y", $tanggal);
        $bulan = date("m", $tanggal);

        $dataLalai = DB::select("
        SELECT absensis.*, IF(abmas > '16:00:00',COALESCE(bonpots.`potongan`, 0),0) AS potongan FROM absensis LEFT JOIN bonpots ON bonpots.`tahun` = YEAR(abhar) AND bonpots.`bulan` = MONTH(abhar)
        WHERE YEAR(abhar) = ?
        AND MONTH(abhar) = ?
        AND karnik = ?
        AND abmas > '16:00:00'        
        ",[$tahun,$bulan,$karnik]);
        return $dataLalai;
    }

    public function Print()
    {
        $user 		= Session::get("data_user");
        $level = $user['level'];
        $karnik = $user['karnik'];
        $pot = Bonpot::select('potongan')->first();
        $bonus = Bonpot::select('bonus')->first();
        
        if ($level == 'pemilik' || $level == 'admin') {
            $query = DB::select("SELECT ab.karnik,kr.karnama,bg.bagnama, ab.abhar, ab.jumlah_hari, ab.jumlah_masuk, ab.potongan, IF(jumlah_masuk=jumlah_hari,COALESCE(bp.`bonus`, '0'),'0') AS bonus,bagpok, (ROUND(bagpok/jumlah_hari*jumlah_masuk) + IF(jumlah_masuk=jumlah_hari,COALESCE(bp.`bonus`, '0'),'0') - ab.potongan) AS total FROM
            (
            SELECT a.karnik,COUNT(a.abhar) AS jumlah_masuk,DAY(LAST_DAY(a.abhar)) AS jumlah_hari, SUM(potongan) AS potongan, a.abhar FROM 
            (SELECT absensis.*, IF(abmas>'16:00:00', bp.`potongan`,0) AS potongan FROM absensis LEFT JOIN bonpots AS bp ON bp.`tahun` = YEAR(absensis.`abhar`) AND bp.`bulan` = MONTH(absensis.`abhar`))a
            GROUP BY karnik, MONTH(abhar)
            )ab
            LEFT JOIN karyawans AS kr ON kr.karnik = ab.karnik 
            LEFT JOIN bagians AS bg ON bg.bagid = kr.bagid
            LEFT JOIN bonpots AS bp ON bp.`tahun` = YEAR(ab.abhar) AND bp.`bulan` = MONTH(ab.abhar)");
        } else {
            $query = DB::select("SELECT ab.karnik,kr.karnama,bg.bagnama, ab.abhar, ab.jumlah_hari, ab.jumlah_masuk, ab.potongan, IF(jumlah_masuk=jumlah_hari,COALESCE(bp.`bonus`, '0'),'0') AS bonus,bagpok, (ROUND(bagpok/jumlah_hari*jumlah_masuk) + IF(jumlah_masuk=jumlah_hari,COALESCE(bp.`bonus`, '0'),'0') - ab.potongan) AS total FROM
            (
            SELECT a.karnik,COUNT(a.abhar) AS jumlah_masuk,DAY(LAST_DAY(a.abhar)) AS jumlah_hari, SUM(potongan) AS potongan, a.abhar FROM 
            (SELECT absensis.*, IF(abmas>'16:00:00', bp.`potongan`,0) AS potongan FROM absensis LEFT JOIN bonpots AS bp ON bp.`tahun` = YEAR(absensis.`abhar`) AND bp.`bulan` = MONTH(absensis.`abhar`))a
            GROUP BY karnik, MONTH(abhar)
            )ab
            LEFT JOIN karyawans AS kr ON kr.karnik = ".$karnik." 
            LEFT JOIN bagians AS bg ON bg.bagid = kr.bagid
            LEFT JOIN bonpots AS bp ON bp.`tahun` = YEAR(ab.abhar) AND bp.`bulan` = MONTH(ab.abhar)");
        }
        $data = array();

        $no=1;
        foreach ($query as $key => $value) {
            $data[$key]['no']=$no++;
            $data[$key]['karnik']=$value->karnik;
            $data[$key]['karnama']=$value->karnama;
            $data[$key]['bagnama']=$value->bagnama;
            $data[$key]['abhar']=$value->abhar;
            $data[$key]['jumlah_hari']=$value->jumlah_hari;
            $data[$key]['jumlah_masuk']=$value->jumlah_masuk;
            $data[$key]['potongan']=$value->potongan;
            $data[$key]['bonus']=$value->bonus;
            $data[$key]['bagpok']=$value->bagpok;
            $data[$key]['total']=$value->total;
        }   
        $send['data_rekap'] = $data;
        // return $data;
        return view('admin.cetak', $send);
        
    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
