<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Absensi;
use App\Models\Karyawan;
use Session;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.admin');
    }

    public function show()
    {
        $absensi = Absensi::latest()->get();
    
        return view('admin.absen',compact('absensi'));
    }
}
