<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SupplyBarang;
use App\Models\Supplier;

class SupplyBarangController extends Controller
{
    
    public function index()
    {
        $supplybarang = SupplyBarang::latest()->paginate(5);
    
        return view('admin.supplybarang.index',compact('supplybarang'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

   
    public function create()
    {
        $supplier = Supplier::all();
        return view('admin.supplybarang.create',compact('supplier'));
    }

    
    public function store(Request $request)
    {
        $request->validate([
            'bkode' => 'required',
            'bnama' => 'required',
            'btgl' => 'required',
            'bjumlah' => 'required',
            'hargabeli' => 'required',
            'supkode' => 'required',
        ]);
    
        SupplyBarang::create($request->all());
     
        return redirect()->route('supplybarang.index')
                        ->with('success','Supply Barang Berhasil ditambahkan.');
    }

   
    public function show($id)
    {
        //
    }

    
    public function edit($bkode)
    {
        $supplier = Supplier::all();
        $supplybarang = SupplyBarang::findOrFail($bkode);
        return view('admin.supplybarang.edit',compact('supplybarang','supplier'));
    }

    
    public function update(Request $request, SupplyBarang $supplybarang)
    {
        $request->validate([
            'bkode' => 'required',
            'bnama' => 'required',
            'btgl' => 'required',
            'bjumlah' => 'required',
            'hargabeli' => 'required',
            'supkode' => 'required',
        ]);

        $supplybarang->update($request->all());

        return redirect()->route('supplybarang.index')
                        ->with('success','Supply Barang Berhasil diubah.');
    }

    
    public function destroy($id)
    {
        //
    }
}
