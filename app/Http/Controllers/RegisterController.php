<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Karyawan;
use DB;
use RealRashid\SweetAlert\Facades\Alert;

class RegisterController extends Controller
{
    public function create()
    {
        $data['karyawan'] = Karyawan::all();
        return view('register.create', $data);
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'karnik' => 'required',
            'name' => 'required',
            'username' => 'required',
            'password' => 'required',
            'level' => 'required'
        ]);
         // auth()->login($users);
        $cek = User::where('karnik', $request['karnik'])->first();
        if($cek == ""){
        User::create($request->all());
        Alert::success('Sukses', 'Anda Berhasil Mendaftar');
        return redirect()->route('akun');

        }else{
        Alert::error('Oops', 'Data yang Anda masukkan double !');
        return redirect()->route('akun');
        }
        // return redirect()->to('akun');
        //$users = User::create(request(['karnik', 'name','username','password','level']));
    }

    public function select2Karyawan(Request $request)
    {
        $search = $request->search;
        $query = DB::table('karyawans')
                ->whereRaw("( karnik like '%".$search."%' or karnama like '%".$search."%' )")
                ->orderBy('karnama','asc')
                ->get();   
                
        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id']=$value->karnik;
            $data[$key]['nama']='( '.$value->karnik.' ) '.$value->karnama;
        }
        // $array[] = array('a'=>'a');
        return response()->json($data);
    }    
}
