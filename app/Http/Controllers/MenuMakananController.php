<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MenuMakanan;
use App\Models\SupplyBarang;

class MenuMakananController extends Controller
{
    
    public function index()
    {
        $menumakanan = MenuMakanan::latest()->paginate(5);
    
        return view('admin.menumakanan.index',compact('menumakanan'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }


    public function create()
    {
        $supplybarang = SupplyBarang::all();
        return view('admin.menumakanan.create',compact('supplybarang'));

    }

    public function store(Request $request)
    {
        $request->validate([
            'menukd' => 'required',
            'bkode' => 'required',
            'stok' => 'required',
            'hargajual' => 'required',
        ]);

        MenuMakanan::create($request->all());
     
        return redirect()->route('menumakanan.index')
                        ->with('success','Menu Makanan Berhasil ditambahkan.');

    }


    public function show($id)
    {
        //
    }


    public function edit($menukd)
    {
        $supplybarang = SupplyBarang::all();
        $menumakanan = MenuMakanan::findOrFail($menukd);
        return view('admin.menumakanan.edit',compact('menumakanan','supplybarang'));
    }


    public function update(Request $request, MenuMakanan $menumakanan)
    {
        $request->validate([
            'menukd' => 'required',
            'bkode' => 'required',
            'stok' => 'required',
            'hargajual' => 'required',
        ]);

        $menumakanan->update($request->all());

        return redirect()->route('menumakanan.index')
                        ->with('success','Menu makanan berhasil diubah.');

    }


    public function destroy(MenuMakanan $menumakanan)
    {
        $menumakanan->delete();
    
        return redirect()->route('menumakanan.index')
                        ->with('success','Data Menu Makanan Berhasil dihapus deleted successfully');

    }
}
