<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class AuthController extends Controller
{
    public function index()
    {
        return view('login');
    }

    public function proses_login(Request $request)
    {
        request()->validate(
            [
                'username' => 'required',
                'password' => 'required',
            ]);
        $kredensil = $request->only('username','password');
            if (Auth::attempt($kredensil)) {
                $user_raw = Auth::user();

                $user = Auth::user()
                ->when($user_raw, function ($query) use ($user_raw) {
                    if ($user_raw->level == 'kasir') {
                        return $query   ->leftjoin('user_cab as uc', 'uc.id_user', '=', 'users.id')
                                        ->leftjoin('cabangs as cab', 'cab.cabkode', '=', 'uc.id_cabang');
                    }
                })
                ->where([['username', '=', $request->username]])
                ->get()->first();
                            // return $user;
                $data = [
                    'id' => $user->id,
                    'name' => $user->name,
                    'username' => $user->username,
                    'email' => $user->email,
                    'level' => $user->level,
                    'cabang' => $user->cabnama,
                    'karnik' => $user->karnik,
                    'cabangkode' => $user->cabkode,
                    'profile_photo_url' => $user->profile_photo_url,
                ];
                // return $user; 
                session::put('data_user', $data);                
                if ($user->level == 'admin') {
                    return redirect()->intended('admin');
                } elseif ($user->level == 'pemilik') {
                    return redirect()->intended('pemilik');
                } elseif ($user->level == 'karyawan') {
                    return redirect()->intended('karyawan');
                } elseif ($user->level == 'kasir') {
                    return redirect()->intended('kasir');
                }
                 return redirect()->intended('/');
            }


        return redirect('login'); 

    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        Auth::logout();
        return Redirect('login');
    }
}
