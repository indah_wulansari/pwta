<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bagian;
use RealRashid\SweetAlert\Facades\Alert;

class BagianController extends Controller
{
    public function index()
    {
        $bagian = Bagian::latest()->paginate(5);
    
        return view('admin.bagian.index',compact('bagian'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('admin.bagian.create');
    }

    public function store(Request $request)
    {
         $request->validate([
            'bagnama' => 'required',
            'bagpok' => 'required'

        ]);
    
        $cek = Bagian::where('bagnama', $request['bagnama'])->first();
        if($cek == ""){
        Bagian::create($request->all());
        Alert::success('Sukses', 'Data Berhasil Ditambahkan');
        return redirect()->route('bagian.index');

        }else{
        Alert::error('Oops', 'Data yang Anda masukkan double !');
        return redirect()->route('bagian.index');
        }
        
    }


    public function show($bagid)
    {
    
    }


    public function edit($bagid)
    {
        $bagian = Bagian::findOrFail($bagid);
        return view('admin.bagian.edit',compact('bagian'));

    }
 
    public function update(Request $request, Bagian $bagian)
    {
        $request->validate([
            'bagnama' => 'required',
            'bagpok' => 'required'
        ]);
                
        $bagian->update($request->all());
    
        Alert::success('Sukses', 'Data Berhasil di Edit');
        return redirect()->route('bagian.index');
                      
    }

    public function destroy(Bagian $bagian)
    {

        $bagian->delete();
        
        Alert::success('Sukses', 'Data Berhasil di Hapus');
        return redirect()->route('bagian.index');
    }

   
}
