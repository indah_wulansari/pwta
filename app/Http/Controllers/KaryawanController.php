<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Karyawan;
use App\Models\Bagian;
use App\Models\Cabang;
use RealRashid\SweetAlert\Facades\Alert;
use Carbon\Carbon;
use Auth;

class KaryawanController extends Controller
{
    
    public function adminkar(Request $request)
    {
     
        $karyawan = Karyawan::latest()->get();
        return view('admin.karyawan.index',compact('karyawan'));
        
    }

    public function index()
    {
        
        return view('karyawan.karyawan');
    }

    
    public function create()
    {
        $cabang = Cabang::all();
        $bagian = Bagian::all();
        $now=Carbon::now()->format('Y');
        $max=$now-17;
        //dd($min);
        return view('admin.karyawan.create',compact('bagian', 'cabang', 'max'));
    }


    public function store(Request $request)
    {
        // return $request->all();
        if ($request->bagid == 1 || $request->bagid == 5) {
            $request->validate([
                'karnik' => 'required',
                'karnama' => 'required',
                'email' => 'required',
                'karjk' => 'required',
                'kartl' => 'required',
                'karal' => 'required',
                'kartelp' => 'required',
                'bagid' => 'required',
                'kartgljoin' => 'required'
            ]);
        }else{
            $request->validate([
                'karnik' => 'required',
                'karnama' => 'required',
                'email' => 'required',
                'karjk' => 'required',
                'kartl' => 'required',
                'karal' => 'required',
                'kartelp' => 'required',
                'bagid' => 'required',
                'cabkode' => 'required',
                'kartgljoin' => 'required'
            ]);

        }
        // return $request->all();
        $cek = Karyawan::find($request['karnik']);
        if($cek == ""){
        Karyawan::create($request->all());
        Alert::success('Sukses', 'Data Berhasil Ditambahkan');
        return redirect()->route('adminkar');

        }else{
        Alert::error('Oops', 'Data yang Anda masukkan double !');
        return redirect()->route('adminkar');
        }
        
    }


    public function show($karnik)
    {
        $bagian = Bagian::all();
        $karyawan =Karyawan::find($karnik);
        return view('admin.karyawan.cetak',compact('karyawan','bagian'));
    }


    public function edit($karnik)
    {
        $cabang = Cabang::all();
        $bagian = Bagian::all();
        $karyawan =Karyawan::findOrFail($karnik);
        return view('admin.karyawan.edit',compact('karyawan','bagian','cabang'));
    }

    public function update(Request $request, Karyawan $karyawan)
    {
        $ck = $request->validate([
            'karnama' => 'required',
            'email' => 'required',
            'karjk' => 'required',
            'kartl' => 'required',
            'karal' => 'required',
            'kartelp' => 'required',
            'bagid' => 'required',
            'kartgljoin' => 'required',
        ]);
        
        $karyawans = Karyawan::find($request->karnik_old);
        $karyawans->update($request->except('_token','_method'));
        Alert::success('Sukses', 'Data Berhasil diubah');
        return redirect()->route('adminkar'); 
    }


    public function destroy(Request $request,Karyawan $karyawan)
    {
        $karyawans = Karyawan::find($request->karnik);
        $karyawans->delete();
        
        Alert::success('Sukses', 'Data Berhasil dihapus');
        return redirect()->route('adminkar');


    }

    public function cetak($karnik)
    {
        $bagian = Bagian::all();
        $karyawan =Karyawan::find($karnik);
        return view('admin.karyawan.cetak',compact('karyawan','bagian'));
    }

    public function Dashkar()
    {
        $karyawan = Karyawan::where('karnik', Auth::user()->karnik)->first();
        return view('karyawan.karyawan', compact('karyawan'));
    }
}
