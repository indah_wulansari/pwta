<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Supplier;


class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $supplier = Supplier::latest()->paginate(5);
    
        return view('admin.supplier.index',compact('supplier'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

   
    public function create()
    {
        return view('admin.supplier.create');
    }

    
    public function store(Request $request)
    {
        $request->validate([
            'supkode' => 'required',
            'supnama' => 'required',
        ]);
    
        Supplier::create($request->all());
     
        return redirect()->route('supplier.index')
                        ->with('success','Supplier created successfully.');
    }

   
    public function show($id)
    {
        //
    }

    
    public function edit($supkode)
    {
        $supplier = Supplier::findOrFail($supkode);
        return view('admin.supplier.edit',compact('supplier'));
    }

    
    public function update(Request $request, Supplier $supplier)
    {
        $request->validate([
            'supnama' => 'required',
        ]);
    
        $supplier->update($request->all());
    
        return redirect()->route('supplier.index')
                        ->with('success','Supplier updated successfully');
    }

   
    public function destroy(Supplier $supplier)
    {
        $supplier->delete();
    
        return redirect()->route('supplier.index')
                        ->with('success','Supplier deleted successfully');
    }
}
