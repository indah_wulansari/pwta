<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kerusakan;
use App\Models\Cabang;

class KerusakanController extends Controller
{
   
    public function index()
    {
        $kerusakan = Kerusakan::latest()->paginate(5);
    
        return view('admin.kerusakan.index',compact('kerusakan'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    
    public function create()
    {
        $cabang = Cabang::all();
        return view('admin.kerusakan.create',compact('cabang'));
    }

    
    public function store(Request $request)
    {
        $request->validate([
            'rusakid' => 'required',
            'cabkode' => 'required',
            'rusaknama' => 'required',
            'rusaktgl' => 'required',
            'rusakjml' => 'required',
            'keterangan' => 'required',
            'hargapenggantian' => 'required',
        ]);
    
        Kerusakan::create($request->all());
     
        return redirect()->route('kerusakan.index')
                        ->with('success','Data Kerusakan berhasil ditambahkan.');
    }

   
    public function show($id)
    {
        //
    }

    
    public function edit($rusakid)
    {
        $cabang = Cabang::all();
        $kerusakan = Kerusakan::findOrFail($rusakid);
        return view('admin.kerusakan.edit',compact('kerusakan','cabang'));

    }

    
    public function update(Request $request, Kerusakan $kerusakan)
    {
        $request->validate([
            'rusakid' => 'required',
            'cabkode' => 'required',
            'rusaknama' => 'required',
            'rusaktgl' => 'required',
            'rusakjml' => 'required',
            'keterangan' => 'required',
            'hargapenggantian' => 'required',
        ]);

        $kerusakan->update($request->all());

        return redirect()->route('kerusakan.index')
                        ->with('success','Data Kerusakan Berhasil diubah.');

    }

    
    public function destroy($id)
    {
        //
    }
}
