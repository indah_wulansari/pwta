<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Karyawan;
use App\Models\Absensi;
use App\Models\Bonpot;
use Carbon\Carbon;
use DB;
use RealRashid\SweetAlert\Facades\Alert;

class PemilikController extends Controller
{
    public function index()
    {
        return view('pemilik.pemilik');
    }

    public function show()
    {
        return view('pemilik.kinerja');
    }

    public function Grafik(Request $request)
    {
        $jumlah = 12;
        $tahun = (isset($request->tahun))? $request->tahun : 2021;
        $karyawan = Karyawan::all();

        $karrajin = DB::select("SELECT a.karnama AS nama, COUNT(a.karnik) AS ter FROM absensis a, karyawans k, bagians b 
        WHERE a.karnik = k.karnik AND k.bagid = b.bagid AND b.bagnama = 'Karyawan'
        GROUP BY a.karnik ORDER BY COUNT(a.karnik) DESC LIMIT 1");

        $karmls = DB::select("SELECT a.karnama AS nama, COUNT(a.karnik) AS ter FROM absensis a, karyawans k, bagians b 
        WHERE a.karnik = k.karnik AND k.bagid = b.bagid AND b.bagnama = 'Karyawan'
        GROUP BY a.karnik ORDER BY COUNT(a.karnik) ASC LIMIT 1");


        $ksrrajin = DB::select("SELECT a.karnama AS nama, COUNT(a.karnik) AS ter FROM absensis a, karyawans k, bagians b 
        WHERE a.karnik = k.karnik AND k.bagid = b.bagid AND b.bagnama = 'Kasir'
        GROUP BY a.karnik ORDER BY COUNT(a.karnik) DESC LIMIT 1");

        $ksrmls = DB::select("SELECT a.karnama AS nama, COUNT(a.karnik) AS ter FROM absensis a, karyawans k, bagians b 
        WHERE a.karnik = k.karnik AND k.bagid = b.bagid AND b.bagnama = 'Kasir'
        GROUP BY a.karnik ORDER BY COUNT(a.karnik) ASC LIMIT 1");

        $jmlkar = DB::select("SELECT COUNT(k.karnik) as kar FROM karyawans k, bagians b WHERE k.bagid = b.bagid");
        $jmlkry = DB::select("SELECT COUNT(k.karnik) AS kar FROM karyawans k, bagians b WHERE k.bagid = b.bagid AND b.bagid = '8'");
        $jmlksr = DB::select("SELECT COUNT(k.karnik) AS kar FROM karyawans k, bagians b WHERE k.bagid = b.bagid AND b.bagid = '4'");
        $jmlcab1 = DB::select("SELECT k.karnama AS kar FROM karyawans k, cabangs c WHERE k.cabkode = c.cabkode AND c.cabkode='CAB-001'");
        $jmlcab2 = DB::select("SELECT k.karnama AS kar FROM karyawans k, cabangs c WHERE k.cabkode = c.cabkode AND c.cabkode='CAB-002'");

        foreach ($karyawan as $key => $value) {
            $data[$key]['name'] = $value->karnama;
            
            for ($i=0; $i < $jumlah; $i++) {
    
                $data_rekap_raw = DB::select("SELECT karnik, MONTH(abhar), COUNT(karnik) AS total FROM
                                            (SELECT * FROM absensis)a
                                            WHERE YEAR(abhar) = ?
                                            AND MONTH(abhar) = ?
                                            AND karnik = ?
                                            GROUP BY karnik, MONTH(abhar)
                                            ",[$tahun, $i+1, $value->karnik]);
                // return $data_rekap_raw;
                // return $data_rekap_raw[0]->total;
                $total[$i] = (isset($data_rekap_raw[0]->total)) ? $data_rekap_raw[0]->total : 0;
            }
            $data[$key]['data'] = $total;
        }
    
        $opt_tahun = Absensi::selectRaw("YEAR(abhar) as tahun")
                    ->groupByRaw("YEAR(abhar)")
                    ->pluck('tahun');

        $send['opt_tahun'] = $opt_tahun;
        // return $opt_tahun;
        // return $data;
        $send['data_rekap'] = $data;
        return view('pemilik.kinerja', $send, compact('karrajin', 'ksrrajin', 'karmls', 'ksrmls', 'jmlkar', 'jmlkry', 'jmlksr', 'jmlcab1', 'jmlcab2'));
    }

    public function Get_Grafik(Request $request)
    {
        $jumlah = 12;
        $tahun = (isset($request->tahun))? $request->tahun : 2021;
        $karyawan = Karyawan::all();

        foreach ($karyawan as $key => $value) {
            $data[$key]['name'] = $value->karnama;
            
            for ($i=0; $i < $jumlah; $i++) {
    
                $data_rekap_raw = DB::select("SELECT karnik, MONTH(abhar), COUNT(karnik) AS total FROM
                                            (SELECT * FROM absensis)a
                                            WHERE YEAR(abhar) = ?
                                            AND MONTH(abhar) = ?
                                            AND karnik = ?
                                            GROUP BY karnik, MONTH(abhar)
                                            ",[$tahun, $i+1, $value->karnik]);
                // return $data_rekap_raw;
                // return $data_rekap_raw[0]->total;
                $total[$i] = (isset($data_rekap_raw[0]->total)) ? $data_rekap_raw[0]->total : 0;
            }
            $data[$key]['data'] = $total;
        }
        return response()->json($data); 
    }

    public function Bonpot()
    {
        $bonpot = Bonpot::where('tahun', Carbon::now()->year)->where('bulan', 1)->get();
        $tahun = Absensi::selectRaw('YEAR(abhar) as tahun')->groupByRaw('YEAR(abhar)')->get();
        $bulan = [
          '1' => 'Januari',
          '2' => 'Februari',
          '3' => 'Maret',
          '4' => 'April',
          '5' => 'Mei',
          '6' => 'Juni',
          '7' => 'Juli',
          '8' => 'Agustus',
          '9' => 'September',
          '10' => 'Oktober',
          '11' => 'November',
          '12' => 'Desembar',
        ];
        // dd($tahun);
        return view('pemilik.bonpot.index',compact('bonpot', 'tahun', 'bulan')); 
    }
    
    public function getBonpot(Request $request)
    {
      $bonpot = Bonpot::where('tahun', $request->tahun)->where('bulan', $request->bulan)->get();
      
      return view('partials.bonpotData', compact('bonpot'));
    }
    public function submitBonpot(Request $request)
    {
      Bonpot::insert($request->except('_token'));
      $bonpot = Bonpot::where('tahun', $request->tahun)->where('bulan', $request->bulan)->get();
      return view('partials.bonpotData', compact('bonpot'));
    }

    public function edit($bpid)
    {
        $bonpot = Bonpot::findOrFail($bpid);
        return view('pemilik.bonpot.edit',compact('bonpot'));

    }
 
    public function update(Request $request, Bonpot $bonpot)
    {
        $request->validate([
            'bonus' => 'required',
            'potongan' => 'required'
        ]);
        Bonpot::where('bpid', $request->id)->update(['bonus' => $request->bonus, 'potongan' => $request->potongan]);
        Alert::success('Sukses', 'Data Berhasil di Edit');
        return redirect()->route('Bonpot');                 
    }
}
