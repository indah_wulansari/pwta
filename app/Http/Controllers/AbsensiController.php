<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Karyawan;
use App\Models\Absensi;
use Carbon\Carbon;
use Session;
use RealRashid\SweetAlert\Facades\Alert;

class AbsensiController extends Controller
{

    public function index()
    {
        return view('karyawan.absensi.index');
    }

    public function hadir()
    {
    }

    public function create()
    {
        $karnik = Session::get('karnik');
        $absensi = Absensi::where('karnik','=',$karnik)->latest()->get();
    
        return view('karyawan.absensi.hadir',compact('absensi','karnik'))
                ->with('karnik',$karnik);
    }

    public function store(Request $request)
    {
        $request->validate([
            'karnik' => 'required',
        ]);

        $cek = Absensi::where('karnik', $request['karnik'])->where('abhar', date('Y-m-d'))->first();
        if($cek ==""){
        $absensi = new Absensi;
        $karnama = Karyawan::where('karnik', $request['karnik'])->first();
        $absensi->karnik = $request['karnik'];
        $absensi->karnama = $karnama->karnama;
        $absensi->abhar = date('Y-m-d');
        $absensi->abmas = date('H:i:s');
        $absensi->abkel = null;
        $absensi->save();

        Alert::success('Sukses', 'Anda Berhasil Absen Masuk');  
        return redirect()->route('absensi.create')
                        ->with('karnik',$request['karnik']);
                     

        }else if($cek->abkel==""){
            $absensi = Absensi::where('karnik', $request['karnik'])->where('abhar', date('Y-m-d'))->first();
            $absensi->abkel = date('H:i:s');
            $absensi->update();

            Alert::success('Sukses', 'Anda Berhasil Absen Keluar'); 
            return redirect()->route('absensi.create')
                        ->with('karnik',$request['karnik']);
             

        }else{
            Alert::error('Oops', 'Anda sudah absen, Ingin Reset Silahkan Hubungi Admin');
            return redirect()->route('absensi.create')
                        ->with('karnik',$request['karnik']);
              
            
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($abid)
    {
        $absensi = Absensi::find($abid);
        $absensi->abmas=null;
        $absensi->update();

        Alert::success('Sukses', 'Absen Masuk Berhasil di Reset');
        return redirect()->route('absen');
    }

    public function update($abid)
    {
        
    }

    public function pulang($abid)
    {
        $absensi = Absensi::find($abid);
        $absensi->abkel=null;
        $absensi->update();

        Alert::success('Sukses', 'Absen Pulang Berhasil di Reset');
        return redirect()->route('absen');
    }


    public function destroy($abid)
    {
        $absensi = Absensi::find($abid);
        $absensi->delete();
        
        Alert::success('Sukses', 'Absen Berhasil di Reset');
        return redirect()->route('absen');
    }
}
