@extends('layout')
@section('title','Halaman Pemilik')
@section('header')
<center><h4>Halaman Bonus dan Potongan</h4></center>
@endsection

@section('content')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <label for="tahun">Tahun</label>
          <select name="tahun" id="tahun" class="form-control">
            @foreach ($tahun as $i)
                <option value="{{$i->tahun}}" {{$i->tahun == \Carbon\Carbon::now()->year ? 'selected' : ''}}>{{$i->tahun}}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="bulan">Bulan</label>
          <select name="bulan" id="bulan" class="form-control">
            @foreach ($bulan as $key => $value)
                <option value="{{$key}}">{{$value}}</option>
            @endforeach
          </select>
        </div>
        
      </div>
        <div class="col-lg-12 margin-tb">
                
        </div>
    </div>

    <br>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
<div id="dataWrapper">
  <table id="databonpot" class="table table-borderes table-striped table-hover databag">
    <thead>
        <tr>
            <th>No</th>
            <th>Bonus</th>
            <th>Potongan</th>
            <th width="280x">AKSI</th>
        </tr>
    </thead>
    <tbody id="dataWrapper">
        @forelse ($bonpot as $bp)
        <tr>
            <td>{{ $bp->bpid }}</td>
            <td>{{ $bp->bonus }}</td>
            <td>{{ $bp->potongan }}</td>
            <td>
                <a href="{{ route('edit', $bp->bpid)}}" class="btn btn-warning btn-sm btn-flat" title="Edit"><i class="fa fa-edit"></i>Edit</a>
            </td>
        </tr>
        @empty
        <tr>
          <td>#</td>
            <td><input type="number" name="bonus" id="bonus" class="form-control" value="0"></td>
            <td><input type="number" name="potongan" id="potongan" class="form-control" value="0"></td>
            <td><button id="btnSimpan" class="btn btn-info btn-sm">Simpan</button></td>
        </tr>
        @endforelse
    </tbody>
  </table>
</div>
<script>
$(document).ready(function () {
  $('#btnSimpan').on('click', function(){
    console.log('submited');
    $.ajax({
      url: "{{route('submitBonpot')}}",
      type: 'POST',
      data: {
        "_token": "{{ csrf_token() }}",
        'tahun': $('#tahun').val(),
        'bulan': $('#bulan').val(),
        'bonus': $('#bonus').val(),
        'potongan': $('#potongan').val(),
      },
      success: function(data){
        Swal.fire({
          text: 'Berhasil!',
          title: 'Berhasil!',
          icon: 'success',
        });

        $('#dataWrapper').html(data);
      }
    });
  });
  $('#databonpot').dataTable({
    "columnDefs": [ {
      "targets": [0, 1, 2, 3],
      "orderable": false,
    } ]
  });
    console.log('lolo');
    
    $('#tahun').add('#bulan').on('change', function(){
      $.ajax({
        url: "{{route('getBonpot')}}",
        type: 'GET',
        data: {
          tahun: $('#tahun').val(),
          bulan: $('#bulan').val()
        },
        success: function(data){
          $('#dataWrapper').html(data);
        }
      })
      
    });
});
</script>
@endsection
