@extends('layout')
@section('title','Halaman Pemilik')
@section('header')
<center><h4>Edit Bonus dan Potongan</h4></center>
@endsection

@section('content')
@if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
        </div>
    @endif
  
    <form action="{{ route('update',$bonpot->bpid) }}" method="POST">
    @csrf
    <div class="row">
        <input type="hidden" value="{{$bonpot->bpid}}" name="id">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Bonus:</strong>
                <input type="text" name="bonus" class="form-control" placeholder="Contoh : 50000 " value="{{ $bonpot->bonus }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Potongan:</strong>
                <input type="text" name="potongan" class="form-control" placeholder="Contoh : 1000" value="{{ $bonpot->potongan }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
    </div>
   
    </form>
@endsection