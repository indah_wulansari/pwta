@extends('layout')
@section('title','Halaman Kinerja Karyawan')
@section('header')
<center><h4>Grafik</h4></center>
@endsection

@section('content')

<div class="row">
    <div class="col-md-3">
      <div class="card card-body bg-danger text-light text-center">
                Sumber Daya Manusia<br>
                @foreach($jmlkar as $row)
                {{$row->kar}}
                @endforeach
        </div>
    </div>
    <div class="col-md-3">
        <div class="card card-body bg-warning text-light text-center">
              Jumlah Karyawan<br>
              @foreach($jmlkry as $row)
              {{$row->kar}}
              @endforeach
        </div>
    </div>
    <div class="col-md-3">
      <div class="card card-body bg-success text-light text-center">
          Jumlah Kasir<br>
          @foreach($jmlksr as $row)
          {{$row->kar}}
          @endforeach
      </div>
    </div> 
    <div class="col-md-3">
        <div class="row">
          <div class="col-12 mb-3">
              <div class="dropdown">
                  <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Karyawan Cabang 1
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      @foreach($jmlcab1 as $row)
                      {{$row->kar}}<br>
                      @endforeach
                  </div>
              </div>
          </div>
          <div class="col-12 mb-3">
              <div class="dropdown">
                      <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Karyawan Cabang 2
                      </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      @foreach($jmlcab2 as $row)
                      {{$row->kar}}<br>
                      @endforeach
                  </div>
              </div>
          </div>
        </div>
    </div>     
</div>
<div class="form-group col-md-3">
    <label for="tahun">Tahun</label>
    <select class="form-control" name="tahun" id="tahun">
        <option> Pilih Tahun</option>
        @foreach ($opt_tahun as $key => $data)
            <option value="{{$opt_tahun[$key]}}">{{$opt_tahun[$key]}}</option>
        @endforeach              
    </select>
</div>
<figure class="highcharts-figure">
    <div id="container"></div>
    <p class="highcharts-description">
                    <div class="col-md-12">
                        <div class="form-group">
                            <h3 class="text-primary">Kesimpulan</h3>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Rangking Paling Atas Karyawan</th>
                                        <th>Rangking Paling Bawah Karyawan</th>
                                        <th>Rangking Paling Atas Kasir</th>
                                        <th>Rangking Paling Bawah Kasir</th>  
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            @foreach($karrajin as $row)
                                            {{$row->nama}}
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach($karmls as $row)
                                            {{$row->nama}}
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach($ksrrajin as $row)
                                            {{$row->nama}}
                                            @endforeach
                                        </td>
                                        <td>@foreach($ksrmls as $row)
                                            {{$row->nama}}
                                            @endforeach
                                        </td>
                                       
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>  
                </div>
    </p>
</figure>

<script>
    var data_rekap = {!!json_encode($data_rekap)!!};

    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Grafik Kinerja Karyawan'
        },
        subtitle: {
            text: 'Angkringan Panjer Wengi'
        },
        xAxis: {
            categories: [
                'Jan',
                'Feb',
                'Mar',
                'Apr',
                'May',
                'Jun',
                'Jul',
                'Aug',
                'Sep',
                'Oct',
                'Nov',
                'Dec'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jumlah Masuk (hari)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name} :</td>' +
                '<td style="padding:0"><b>{point.y} hari</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series:data_rekap
    });

    $(document).on('change','#tahun',function (e) {
        e.preventDefault();
        var tahun = $(this).val();
        // console.log(tahun);
        $.ajax({
            type: "get",
            url: "{{route('Get_Grafik')}}?tahun="+tahun,
            dataType: "json",
            success: function (data) {
                console.log(data);

                Highcharts.chart('container', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Grafik Kinerja Karyawan'
                    },
                    subtitle: {
                        text: 'Angkringan Panjer Wengi'
                    },
                    xAxis: {
                        categories: [
                            'Jan',
                            'Feb',
                            'Mar',
                            'Apr',
                            'May',
                            'Jun',
                            'Jul',
                            'Aug',
                            'Sep',
                            'Oct',
                            'Nov',
                            'Dec'
                        ],
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Jumlah Masuk (hari)'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name} :</td>' +
                            '<td style="padding:0"><b>{point.y} hari</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series:data
                });
                
            }
        });
    });

</script>
@endsection

@push('script')
<script>

</script>
@endpush