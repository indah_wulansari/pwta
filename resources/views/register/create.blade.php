@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h3>Halaman Kelola Akun</h3></center>
@endsection

@section('content')
    <form method="POST" action="register">
        {{ csrf_field() }}
        <div class="row">
        <div class="col-md-12">
        <div class="form-group">
            <label for="karnik">NIK:</label>
            <!-- <input type="text" class="form-control" id="karnik" name="karnik"> -->
            <select class="form-control" id="karnik" name="karnik">
            <option value="">Pilih Karyawan</option>
                @foreach ($karyawan as $datas)
                    <option value="{{ $datas -> karnik}}" data-karnama="{{ $datas -> karnama}}">( {{ $datas -> karnik  }} ) {{ $datas-> karnama }}</option>
                @endforeach
            </select>
            <input type="text" class="form-control" id="name" name="name">
        </div>
        </div>


        <div class="col-md-12">
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" class="form-control" id="username" name="username">
        </div>
        </div>

        <div class="form-group col-md-12">
            <label for="level">Level</label>
            <select class="form-control" name="level" id="level">
                <option> Pilih Level</option>
                <option value="admin"> Admin </option>
                <option value="karyawan"> Karyawan </option>
                <option value="kasir"> Kasir </option>
                <option value="pemilik"> Pemilik </option>
            </select>
        </div>

        <div class="col-md-12">
        <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" class="form-control" id="password" name="password">
        </div>
        </div>

        <div class="col-md-12">
        <center><div class="form-group">
            <button style="cursor:pointer" type="submit" class="btn btn-info">Simpan</button>
        </div></center>
        </div>
        </div>
    </form>


<script> 
$(document).ready(function () {

    $('#karnik').select2();

    $('#karnik').change(function (e) { 
        e.preventDefault();
        var nama = $('#karnik :checked').data('karnama');
        $('#name').val(nama);
        console.log($('#karnik :checked').data('karnik'));
         console.log('kosos');       
    });
    
    // $('#karnik').on('change', '#karnik', function (e) {
    //     e.preventDefault();        
    //     console.log($(this).data('id'));
    //     console.log('kosos');
    // });

    $(document).on('click', '.editDetail', function (e) {
        e.preventDefault();
        var nama = $(this).data('karnama');
        var abhar = $(this).data('abhar');
        var karnik = $(this).data('karnik');
        var jumlah_hari = $(this).data('jumlah_hari');
        var jumlah_masuk = $(this).data('jumlah_masuk');
        
        var url_panitia = "{{url('getKelalaian/')}}/"+karnik+"/"+abhar;
        $.getJSON(url_panitia , function (data) {
            console.log(data);
            $.each(data, function (i, v) {
                var row =  '<tr><th>' + ++i + '</th>';
                    row += '<th>'+v.abhar+'</th>';
                    row += '<th>Telat masuk</th>';
                    row += '<th>'+v.potongan+'</th></tr>';

                $('.row_lalai').append(row);  
            });
        });

        $('#nama_detail').html(nama);
        $('#jh_detail').html(jumlah_hari);
        $('#jm_detail').html(jumlah_masuk);
        
        // console.log('lll');

        $('#modal_gaji').modal({
            backdrop: 'static',
            keyboard: false, // to prevent closing with Esc button (if you want this too)
            show: true  
        })
                
    });


    $('#modal_review').modal({
    backdrop: 'static',
    keyboard: false, // to prevent closing with Esc button (if you want this too)
    show: true  
})

    console.log('lolo');
});
</script>    
@endsection

