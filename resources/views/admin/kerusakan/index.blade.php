@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h4>Daftar Kerusakan</h4></center>
@endsection

@section('content')

<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-info btn-sm" href="{{ route('kerusakan.create') }}">Tambah Data Kerusakan</a>
            </div>
        </div>
    </div>

    </br>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-borderes table-striped table-hover">
        <tr>
            <th>Kode Kerusakan</th>
            <th>Cabang</th>
            <th>Nama Barang</th>
            <th>Tanggal</th>
            <th>Jumlah</th>
            <th>keterangan</th>
            <th>Harga</th>
            <th>AKSI</th>
        </tr>
        @foreach ($kerusakan as $rusk)
        <tr>
            <td>{{ $rusk->rusakid }}</td>
            <td>{{ $rusk->cabang->cabnama }}</td>
            <td>{{ $rusk->rusaknama }}</td>
            <td>{{ $rusk->rusaktgl }}</td>
            <td>{{ $rusk->rusakjml }}</td>
            <td>{{ $rusk->keterangan }}</td>
            <td>{{ $rusk->hargapenggantian }}</td>
            <td>
                <form action="{{ route('kerusakan.destroy', $rusk->rusakid )}}" method="post">
                <a href="{{ route('kerusakan.edit', $rusk->rusakid )}}" class="btn btn-warning btn-sm">Edit</a>
                    @csrf
                    
                </form>
            </td>
        </tr>
        @endforeach
    </table>  
@endsection

