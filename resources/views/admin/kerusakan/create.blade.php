@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h4>Tambah Data Kerusakan</h4></center>
@endsection

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
    </div>
@endif
   
<form action="{{ route('kerusakan.store') }}" method="POST">
    @csrf
  
     <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <strong>Kode Kerusakan</strong>
                <input type="text" name="rusakid" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-md-4">
        <div class="form-group">
            <strong>Cabang</strong>
            <select name="cabkode" id="cabang" class="form-control">
            <option value="">Pilih Cabang</option>
            @foreach ($cabang as $data)
                <option value="{{ $data -> cabkode  }}">{{ $data-> cabnama }}</option>
                @endforeach
            </select>            
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <strong>Nama Barang</strong>
                <input type="text" name="rusaknama" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <strong>Tanggal</strong>
                <input type="date" name="rusaktgl" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <strong>Jumlah</strong>
                <input type="text" name="rusakjml" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <strong>keterangan</strong>
                <input type="text" name="keterangan" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <strong>Harga</strong>
                <input type="text" name="hargapenggantian" class="form-control" placeholder="">
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
   
</form>
@endsection