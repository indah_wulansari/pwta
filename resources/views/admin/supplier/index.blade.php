@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h4>Daftar Supplier</h4></center>
@endsection

@section('content')

<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <a class="btn btn-info btn-sm" href="{{ route('supplier.create') }}">Tambah Supplier</a>
            </div>
        </div>
    </div>

    </br>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-borderes table-striped table-hover">
        <tr>
            <th>Kode Supplier</th>
            <th>Nama Supplier</th>
            <th>AKSI</th>
        </tr>
        @foreach ($supplier as $sup)
        <tr>
            <td>{{ $sup->supkode }}</td>
            <td>{{ $sup->supnama }}</td>
            <td>
                <form action="{{ route('supplier.destroy', $sup->supkode)}}" method="post">
                <a href="{{ route('supplier.edit', $sup->supkode)}}" class="btn btn-warning btn-sm">Edit</a>
                    @csrf
                    @method('DELETE')      
                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>  
@endsection

