<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body onload="window.print()">
<div>
   <center>Laporan Gaji Karyawan</center>
   <center>Angkringan Panjer Wengi</center>
    </br>   
    <table id="tb_gaji" class="table table-borderes table-striped table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>NIK</th>
                <th>Nama</th>
                <th>Bagian</th>
                <th>Tanggal</th>
                <th>Potongan</th>
                <th>Bonus</th>
                <th>Gaji Pokok</th> 
                <th>Gaji Total</th>
            </tr>
        </thead>
            @foreach($data_rekap as $key => $value)
                <tr>
                    <td>{{++$key}}</td>
                    <td>{{$value['karnik']}}</td>
                    <td>{{$value['karnama']}}</td>
                    <td>{{$value['bagnama']}}</td>
                    <td>{{$value['abhar']}}</td>
                    <td>{{$value['potongan']}}</td>
                    <td>{{$value['bonus']}}</td>
                    <td>{{$value['bagpok']}}</td> 
                    <td>{{$value['total']}}</td>
                </tr>
            @endforeach
        <tbody>

        </tbody>
    </table>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</html>