@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h4>Edit Karyawan</h4></center>
@endsection

@section('content')
@if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
        </div>
    @endif
  
    <form action="{{ route('datakar.update',$karyawan->karnik) }}" method="POST">
        @csrf
        @method('patch')
   
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <strong>NIK:</strong>
                <input type="text" disabled name="karnik" class="form-control"  value="{{ $karyawan->karnik }}">
                <input type="text" hidden name="karnik_old" class="form-control"  value="{{ $karyawan->karnik }}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <strong>NAMA:</strong>
                <input type="text" name="karnama" class="form-control"  value="{{ $karyawan->karnama }}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <strong>EMAIL</strong>
                <input type="text" name="email" class="form-control"  value="{{ $karyawan->email }}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <strong>JENIS KELAMIN:</strong>
                <input type="text" readonly name="karjk" class="form-control"  value="{{ $karyawan->karjk }}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <strong>TTL:</strong>
                <input type="text" readonly name="kartl" class="form-control"  value="{{ $karyawan->kartl }}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <strong>ALAMAT:</strong>
                <input type="text" name="karal" class="form-control" value="{{ $karyawan->karal }}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <strong>TELEPON:</strong>
                <input type="text" name="kartelp" class="form-control" value="{{ $karyawan->kartelp }}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
            <strong>BAGIAN</strong>
            <select name="bagid" id="bagian" class="form-control">
                <option value="">Pilih Bagian</option>
                @foreach ($bagian as $data)
                    <option value="{{ $data -> bagid  }}">{{ $data-> bagnama }}</option>
                @endforeach
            </select>            
            </div>
        </div>
        <div class="col-md-6" id="col_cabang">
            <div class="form-group">
            <strong>CABANG</strong>
            <select name="cabkode" id="cabang" class="form-control">
                <option value="">Pilih Cabang</option>
                @foreach ($cabang as $datas)
                    <option value="{{ $datas -> cabkode  }}">{{ $datas-> cabkode }}</option>
                @endforeach
            </select>            
            </div>
        </div>
        
        <div class=" col-md-6">
            <div class="form-group">
                <strong>TANGGAL JOIN :</strong>
                <input type="text" readonly name="kartgljoin" class="form-control" placeholder="Contoh : 1000000" value="{{ $karyawan->kartgljoin }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
    </div>
   
    </form>

<script>
$('#bagian').change(function (e) { 
    e.preventDefault();
    var id_bagian = $('#bagian :checked').val();
    if ( id_bagian == 1 || id_bagian == 5) {
        $('#col_cabang').attr('hidden', true).attr('disabled',true);
    } else {
        $('#col_cabang').attr('hidden', false).attr('disabled',false);
    }
    
    
});
</script>
@endsection