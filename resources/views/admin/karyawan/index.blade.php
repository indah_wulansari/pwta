@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h4>Daftar Karyawan</h4></center>
@endsection

@section('content')

<div class="row">
        <div class="col-lg-12 margin-tb">
                <a class="btn btn-info btn-sm" href="{{ route('datakar.create') }}">Tambah Karyawan</a>
        </div>
    </div>

    </br>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @elseif ($message = Session::get('error'))
    <div class="alert alert-danger">
            <p>{{ $message }}</p>
        </div>
    @endif


   <div class="table-responsive">
    <table id="datakary" class="table table-borderes table-striped table-hover" height="550px">
        <thead>
            <tr>
                <th>NIK</th>
                <th>QR Code</th>
                <th>NAMA</th>
                <th>EMAIL</th>
                <th>GENDER</th>
                <th>TTL</th>
                <th>ALAMAT</th>
                <th>TELEPON</th>
                <th>BAGIAN</th>
                <th>CABANG</th>
                <th>TANGGAL JOIN</th>
                <th>AKSI</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($karyawan as $kar)
            <tr >
                <td>{{ $kar->karnik }} </td>
                <td style="mt-4 mb-4">{!! QrCode::size(100)->generate($kar->karnik); !!}</td>
                <td>{{ $kar->karnama }}</td>
                <td>{{ $kar->email }}</td>
                <td>{{ $kar->karjk }}</td>
                <td>{{ $kar->kartl }}</td>
                <td>{{ $kar->karal }}</td>
                <td>{{ $kar->kartelp }}</td>
                <td>{{ $kar->bagian->bagnama }}</td>
                <td>{{ (isset($kar->cabang->cabnama)) ? $kar->cabang->cabnama : '-' }}</td>
                <td>{{ $kar->kartgljoin }}</td>
                <td>
                    <form action="{{ route('datakar.destroy', $kar->karnik)}}" method="post">
                        <a style="text-decoration:none; color:#fff" href="{{ route('datakar.edit', $kar->karnik)}}" class="btn btn-warning btn-sm btn-flat" title="Edit"><i class="fa fa-edit"></i></a>
                        @csrf
                        @method('DELETE')      
                        <input type="text" name="karnik" hidden value="{{$kar->karnik}}">
                        <button type="submit" class="btn btn-danger btn-sm btn-flat" title="Hapus"><i class="fa fa-trash"></i></button>
                        <a style="text-decoration:none; color:#fff" href="{{ route('datakar.show', $kar->karnik)}}" class="btn btn-success btn-sm btn-flat" target="_blank" title="Cetak"><i class="fa fa-print"></i></a>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    </div>
    <script> 
        $(document).ready(function () {
        $('#datakary').dataTable({

        });
        console.log('lolo');
        });
    </script> 
@endsection