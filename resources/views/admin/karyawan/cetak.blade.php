<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body onload="window.print()">
<div class="p-2"style="border:1px solid #000000; width:600px; height:auto;">
   <center>Kartu Tanda Karyawan</center>
   <center>Angkringan Panjer Wengi</center>
    </br> 
    <div class="mr-2" style="float:right">{!! QrCode::size(100)->generate($karyawan->karnik); !!}</div>
    <table>
        <tr>
            <td>Nama</td>
            <td>: {{ $karyawan->karnama }}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>: {{ $karyawan->email }}</td>
        </tr>
        <tr>
            <td>TTL</td>
            <td>: {{ $karyawan->kartl }}</td>
        </tr>
        <tr>
            <td> Jenis Kelamin</td>
            <td>: {{ $karyawan->karjk }}</td>
        </tr>
        <tr>
            <td> ALamat </td>
            <td>: {{ $karyawan->karal }}</td>
        </tr>
        <tr>
            <td> Telepon</td>
            <td>: {{ $karyawan->kartelp }}</td>
        </tr>
        <tr>
            <td>Bagian</td>
            <td> : {{ $karyawan->bagian->bagnama }}</td>
        </tr>
        <tr>
            <td> Tanggal Join</td>
            <td>: {{ $karyawan->kartgljoin }}</td>
        </tr>
        
    </table>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</html>