@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h4>Tambah Karyawan</h4></center>
@endsection

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
    </div>
@endif
   
<form action="{{ route('datakar.store') }}" method="POST">
    @csrf
  
     <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <strong>NIK</strong>
                <input type="text" name="karnik" class="form-control" placeholder="Contoh : 3507987654321234">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <strong>NAMA</strong>
                <input type="text" name="karnama" class="form-control" placeholder="Contoh : Indah Wulansari">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <strong>EMAIL</strong>
                <input type="text" name="email" class="form-control" placeholder="Contoh : indah@gmail.com">
            </div>
        </div>
        <div class="form-group col-md-6">
            <label for="karjk">Jenis Kelamin</label>
            <select class="form-control" name="karjk" id="karjk">
                <option value="">Pilih Gender</option>
                <option> Pria</option>
                <option> Wanita </option>
            </select>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <strong>TTL</strong>
                <input type="date" name="kartl" class="form-control" placeholder="Contoh : 09/08/1999" max="{{$max}}-12-31">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <strong>ALAMAT</strong>
                <input type="text" name="karal" class="form-control" placeholder="Contoh : Bujel">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <strong>TELEPON</strong>
                <input type="text" name="kartelp" class="form-control" placeholder="Contoh : 089123456789">
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
            <strong>BAGIAN</strong>
            <select name="bagid" id="bagian" class="form-control">
                <option value="">Pilih Bagian</option>
                @foreach ($bagian as $data)
                    <option value="{{ $data -> bagid  }}">{{ $data-> bagnama }}</option>
                @endforeach
            </select>            
            </div>
        </div>

        <div class="col-md-6" id="col_cabang">
            <div class="form-group">
            <strong>CABANG</strong>
            <select name="cabkode" id="cabang" class="form-control">
                <option value="">Pilih Cabang</option>
                @foreach ($cabang as $datas)
                    <option value="{{ $datas -> cabkode  }}">{{ $datas-> cabnama }}</option>
                @endforeach
            </select>            
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <strong>TANGGAL JOIN</strong>
                <input type="date" name="kartgljoin" class="form-control" placeholder="Contoh : 04/02/2021" min="2017-01-01">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
    </div>
   
</form>

<script>
$('#bagian').change(function (e) { 
    e.preventDefault();
    var id_bagian = $('#bagian :checked').val();
    if ( id_bagian == 1 || id_bagian == 5) {
        $('#col_cabang').attr('hidden', true);
    } else {
        $('#col_cabang').attr('hidden', false);
    }
    
    
});
</script>
@endsection