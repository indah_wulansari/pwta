@extends('layout')
@section('title','Halaman Kehadiran')
@section('header')
<center><h4>Daftar Hadir</h4></center>
@endsection

@section('content')

<div class="table-responsive">
    <table id="dataabsa" class="table table-borderes table-striped table-hover" height="550px">
        <thead>
            <tr>
                <th>NIK</th>
                <th>NAMA</th>
                <th>TANGGAL</th>
                <th>MASUK</th>
                <th>PULANG</th>
                <th>AKSI</th>

            </tr>
        <thead>
        <tbody>
        @foreach ($absensi as $abs)
        <tr >
            <td>{{ $abs->karnik }} </td>
            <td>{{ $abs->karnama }}</td>
            <td>{{ $abs->abhar }}</td>
            <td>{{ $abs->abmas }}</td>
            <td>{{ $abs->abkel }}</td>
            <td>
                <form action="{{ route('absensi.destroy', $abs->abid)}}" method="post">
                <a style="text-decoration:none; color:#fff" href="{{ route('absensi.edit', $abs->abid)}}" class="btn btn-warning btn-sm btn-flat" title="Reset Absen Masuk"><i class="fa fa-edit"></i></a>
                <a style="text-decoration:none; color:#fff" href="{{ route('pulang/', $abs->abid)}}" class="btn btn-success btn-sm btn-flat" title="Reset Absen Pulang"><i class="fa fa-edit"></i></a>
                    @csrf 
                     @method('DELETE')      
                    <button type="submit" class="btn btn-danger btn-sm btn-flat" title="Reset Absen"><i class="fa fa-trash"></i></button>
                </form>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
    
    <script>
$(document).ready(function () {
    $('#dataabsa').dataTable();
    console.log('lolo');
});
</script>
@endsection