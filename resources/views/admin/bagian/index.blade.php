@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h4>Halaman Bagian</h4></center>
@endsection

@section('content')

<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <a class="btn btn-info btn-sm" href="{{ route('bagian.create') }}">Tambah Bagian</a>
            </div>
        </div>
    </div>

    </br>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
<table id="databag" class="table table-borderes table-striped table-hover databag">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Bagian</th>
                <th>Gaji Pokok</th>
                <th width="280x">AKSI</th>
            </tr>
        </thead>
    <tbody>
        @foreach ($bagian as $bag)
        <tr>
            <td>{{ $bag->bagid }}</td>
            <td>{{ $bag->bagnama }}</td>
            <td>{{ $bag->bagpok }}</td>
            <td>
                <form action="{{ route('bagian.destroy', $bag->bagid)}}" method="post">
                <a href="{{ route('bagian.edit', $bag->bagid)}}" class="btn btn-warning btn-sm btn-flat" title="Edit"><i class="fa fa-edit"></i>Edit</a>
                    @csrf 
                     @method('DELETE')      
                    <button type="submit" class="btn btn-danger btn-sm btn-flat" title="Hapus"><i class="fa fa-trash"></i>Hapus</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<script>
$(document).ready(function () {
    $('#databag').dataTable();
    console.log('lolo');
});
</script>
@endsection
