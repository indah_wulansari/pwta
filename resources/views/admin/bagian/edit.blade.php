@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h4>Edit Bagian</h4></center>
@endsection

@section('content')
@if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
        </div>
    @endif
  
    <form action="{{ route('bagian.update',$bagian->bagid) }}" method="POST">
        @csrf
        @method('PUT')
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nama Bagian:</strong>
                <input type="text" name="bagnama" class="form-control" placeholder="Contoh : Kasir " value="{{ $bagian->bagnama }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Gaji Pokok:</strong>
                <input type="text" name="bagpok" class="form-control" placeholder="Contoh : 1000000" value="{{ $bagian->bagpok }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
    </div>
   
    </form>
@endsection