@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h4>Edit Data Menu Makanan</h4></center>
@endsection

@section('content')
@if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
        </div>
    @endif

    <form action="{{ route('menumakanan.update', $menumakanan->menukd) }}" method="POST">
    @csrf    
    @method('PUT')
  
     <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <strong>Kode Menu</strong>
                <input type="text" name="menukd" class="form-control" value="{{ $menumakanan->menukd }}">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
            <strong>Nama Menu</strong>
            <select name="bkode" id="supplybarang" class="form-control">
                <option value="">Pilih Menu</option>
                @foreach ($supplybarang as $data)
                    <option value="{{ $data -> bkode  }}">{{ $data-> bnama }}</option>
                @endforeach
            </select>            
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <strong>Stok</strong>
                <input type="text" name="stok" class="form-control" value="{{ $menumakanan->stok }}">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <strong>Harga Jual</strong>
                <input type="text" name="hargajual" class="form-control" value="{{ $menumakanan->hargajual }}">
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>

    </form>
@endsection