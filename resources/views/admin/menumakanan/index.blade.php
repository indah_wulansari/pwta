@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h4>Daftar Menu Makanan</h4></center>
@endsection

@section('content')

<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-info btn-sm" href="{{ route('menumakanan.create') }}">Tambah Data Menu Makanan</a>
            </div>
        </div>
    </div>

    </br>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-borderes table-striped table-hover">
        <tr>
            <th>Kode Menu</th>
            <th>Menu</th>
            <th>Stok</th>
            <th>Harga Jual</th>
            <th>Harga Beli</th>
            <th>AKSI</th>
        </tr>
        @foreach ($menumakanan as $mm)
        <tr>
            <td>{{ $mm->menukd }}</td>
            <td>{{ $mm->spbarang->bnama }}</td>
            <td>{{ $mm->stok }}</td>
            <td>{{ $mm->hargajual }}</td>
            <td>{{ $mm->spbarang->hargabeli }}</td>
            <td>
                <form action="{{ route('menumakanan.destroy', $mm->menukd )}}" method="post">
                <a href="{{ route('menumakanan.edit', $mm->menukd )}}" class="btn btn-warning btn-sm">Edit</a>
                    @csrf
                    @method('DELETE')      
                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>  
@endsection

