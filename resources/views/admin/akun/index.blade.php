@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h3>Daftar Akun</h3></center>
@endsection

@section('content')
    <a href="{{ route('register') }}" class="btn btn-info btn-sm">Tambah Akun</a><br>

    <br/>

    <table id="dataakun" class="table table-borderes table-striped table-hover">
        <thead>
            <tr>
                <th>NIK</th>
                <th>Nama</th>
                <th>Username</th>
                <th>Level</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{ $user->karnik }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->username }}</td>
                <td>{{ $user->level }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <script>
$(document).ready(function () {
    $('#dataakun').dataTable({
        // to limit records
pageLength: 5,
    });
});
</script>
@endsection
