@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h4>Daftar Barang</h4></center>
@endsection

@section('content')

<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-info btn-sm" href="{{ route('supplybarang.create') }}">Tambah Barang</a>
            </div>
        </div>
    </div>

    </br>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-borderes table-striped table-hover">
        <tr>
            <th>Kode Barang</th>
            <th>Nama Barang</th>
            <th>Tanggal</th>
            <th>Jumlah</th>
            <th>Harga Beli</th>
            <th>Supplier</th>
            <th>AKSI</th>
        </tr>
        @foreach ($supplybarang as $suppb)
        <tr>
            <td>{{ $suppb->bkode }}</td>
            <td>{{ $suppb->bnama }}</td>
            <td>{{ $suppb->btgl }}</td>
            <td>{{ $suppb->bjumlah }}</td>
            <td>{{ $suppb->hargabeli }}</td>
            <td>{{ $suppb->supplier->supnama }}</td>
            <td>
                <form action="{{ route('supplybarang.destroy', $suppb->bkode)}}" method="post">
                <a href="{{ route('supplybarang.edit', $suppb->bkode)}}" class="btn btn-warning btn-sm">Edit</a>
                    @csrf
                    
                </form>
            </td>
        </tr>
        @endforeach
    </table>  
@endsection

