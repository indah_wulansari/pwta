@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h4>Tambah Barang</h4></center>
@endsection

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
    </div>
@endif
   
<form action="{{ route('supplybarang.store') }}" method="POST">
    @csrf
  
     <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <strong>Kode Barang</strong>
                <input type="text" name="bkode" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <strong>Nama Barang</strong>
                <input type="text" name="bnama" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <strong>Tanggal</strong>
                <input type="date" name="btgl" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <strong>Jumlah</strong>
                <input type="text" name="bjumlah" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <strong>Harga Beli</strong>
                <input type="text" name="hargabeli" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
            <strong>Suppplier</strong>
            <select name="supkode" id="supplier" class="form-control">
                <option value="">Pilih Supplier</option>
                @foreach ($supplier as $data)
                    <option value="{{ $data -> supkode  }}">{{ $data-> supnama }}</option>
                @endforeach
            </select>            
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
   
</form>
@endsection