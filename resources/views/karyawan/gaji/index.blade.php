@extends('layout')
@section('title','Halaman Admin')
@section('header')
<center><h4>Selamat Datang Admin</h4></center>
@endsection

@section('content')
<div class="table-responsive">
    <table id="tb_gaji" class="table table-borderes table-striped table-hover">
            <thead>
                <tr>
                    <th>No</th>
                    <th>NIK</th>
                    <th>Nama</th>
                    <th>Bagian</th>
                    <th>Tanggal</th>
                    <th>Potongan</th>
                    <th>Bonus</th>
                    <th>Gaji Pokok</th> 
                    <th>Gaji Total</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        <tbody>
        </tbody>
    </table>
</div>

<!-- Modal -->
<div class="modal fade" id="modal_gaji" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Detail</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <h3 class="text-primary">Nama</h3>
                            <h3 id="nama_detail"></h3>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <h3 class="text-primary">Jumlah Hari</h3>
                            <h3 id="jh_detail"></h3>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <h3 class="text-primary">Jumlah Masuk</h3>
                            <h3 id="jm_detail"></h3>
                        </div>
                    </div>  
                    <div class="col-md-12">
                        <div class="form-group">
                            <h3 class="text-primary">Detail Kelalaian</h3>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>tanggal</th>
                                        <th>keterangan</th>
                                        <th>denda</th>
                                    </tr>
                                </thead>
                                <tbody class="row_lalai">                            
                                </tbody>
                            </table>
                        </div>
                    </div>  
                </div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
				<!-- <button type="button" class="btn btn-primary">Save changes</button> -->
			</div>
		</div>
	</div>
</div>

<script> 
    $(document).ready(function () {

        var table = $('#tb_gaji').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('DTGaji') }}",
            language: {
                            processing: "Sedang diproses...<img height=50 src=''>"
                        },
            columns: [
                {
                    data: 'no',
                    name: 'no',
                    searchable : false,
                    orderable:true
                }, {
                    data: 'karnik',
                    name: 'karnik',
                    orderable:true
                },{
                    data: 'karnama',
                    name: 'karnama',
                    orderable:true
                },{
                    data: 'bagnama',
                    name: 'bagnama',
                    orderable:true
                },{
                    data: 'abhar',
                    name: 'abhar',
                    orderable:true
                },{
                    data: 'potongan',
                    name: 'potongan',
                    orderable:true
                },{
                    data: 'bonus',
                    name: 'bonus',
                    orderable:true
                },{
                    data: 'bagpok',
                    name: 'bagpok',
                    orderable:true
                },{
                    data: 'total',
                    name: 'total',
                    orderable:true
                },{
                    data: 'action',
                    name: 'aksi',
                    orderable: false,
                    searchable: false
                },
            ],
            columnDefs: [
                { className: 'text-center', targets: [0,1,2] },
            ]
        });        

        $(document).on('click', '.editDetail', function (e) {
            e.preventDefault();
            var nama = $(this).data('karnama');
            var abhar = $(this).data('abhar');
            var karnik = $(this).data('karnik');
            var jumlah_hari = $(this).data('jumlah_hari');
            var jumlah_masuk = $(this).data('jumlah_masuk');
            $('.row_lalai').html('');  
            
            var url_panitia = "{{url('getKelalaian/')}}/"+karnik+"/"+abhar;
            $.getJSON(url_panitia , function (data) {
                console.log(data);
                $.each(data, function (i, v) {
                   var row =  '<tr><th>' + ++i + '</th>';
                       row += '<th>'+v.abhar+'</th>';
                       row += '<th>Telat masuk</th>';
                       row += '<th>'+v.potongan+'</th></tr>';

                    $('.row_lalai').append(row);  
                });
            });

            $('#nama_detail').html(nama);
            $('#jh_detail').html(jumlah_hari);
            $('#jm_detail').html(jumlah_masuk);
            
            // console.log('lll');

            $('#modal_gaji').modal({
                backdrop: 'static',
                keyboard: false, // to prevent closing with Esc button (if you want this too)
                show: true  
            })
                   
        });


        $('#modal_review').modal({
        backdrop: 'static',
        keyboard: false, // to prevent closing with Esc button (if you want this too)
        show: true  
    })

        console.log('lolo');
    });
    </script> 
@endsection
