@extends('layout')
@section('title','Halaman Karyawan')
@section('header')
<center><h4>Halaman Karyawan</h4></center>
@endsection

@section('content')
    <center><h1>Selamat Datang {{Auth::user()->name}}</h1></center>
    </br>
    <body>
<div>
   <center>Kartu Tanda Karyawan</center>
   <center>Angkringan Panjer Wengi</center>
    </br> 
    <div class="mr-2" style="float:right">{!! QrCode::size(100)->generate($karyawan->karnik); !!}</div>
    <table>
        <tr>
            <td>Nama</td>
            <td>: {{ $karyawan->karnama }}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>: {{ $karyawan->email }}</td>
        </tr>
        <tr>
            <td>TTL</td>
            <td>: {{ $karyawan->kartl }}</td>
        </tr>
        <tr>
            <td> Jenis Kelamin</td>
            <td>: {{ $karyawan->karjk }}</td>
        </tr>
        <tr>
            <td> ALamat </td>
            <td>: {{ $karyawan->karal }}</td>
        </tr>
        <tr>
            <td> Telepon</td>
            <td>: {{ $karyawan->kartelp }}</td>
        </tr>
        <tr>
            <td>Bagian</td>
            <td> : {{ $karyawan->bagian->bagnama }}</td>
        </tr>
        <tr>
            <td> Tanggal Join</td>
            <td>: {{ $karyawan->kartgljoin }}</td>
        </tr>
        
    </table>
</div>

</body>
</br>
<center><a style="text-decoration:none; color:#fff" href="{{ route('datakar.show', $karyawan->karnik)}}" class="btn btn-success btn-sm btn-flat" target="_blank" title="Cetak"><i class="fa fa-print"></i>Cetak</a></center>

    
@endsection