@extends('layout')
@section('title','Halaman Kehadiran')
@section('header')
<center><h4>Daftar Hadir</h4></center>
@endsection

@section('content')
    </br>
   
    @if ($message = Session::get('in'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @elseif ($message = Session::get('out'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @elseif ($message = Session::get('error'))
        <div class="alert alert-danger">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table id="dataabs" class="table table-borderes table-striped table-hover" height="550px">
        <thead>
            <tr>
                <th>NIK</th>
                <th>NAMA</th>
                <th>TANGGAL</th>
                <th>MASUK</th>
                <th>PULANG</th>

            </tr>
        <thead>
        <tbody>
        @foreach ($absensi as $abs)
        <tr >
            <td>{{ $abs->karnik }} </td>
            <td>{{ $abs->karnama }}</td>
            <td>{{ $abs->abhar }}</td>
            <td>{{ $abs->abmas }}</td>
            <td>{{ $abs->abkel }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
    
    <script>
$(document).ready(function () {
    $('#dataabs').dataTable();
    console.log('lolo');
});
</script>
@endsection