<table id="databonpot" class="table table-borderes table-striped table-hover databag">
  <thead>
      <tr>
          <th>No</th>
          <th>Bonus</th>
          <th>Potongan</th>
          <th width="280x">AKSI</th>
      </tr>
  </thead>
  <tbody id="dataWrapper">
      @forelse ($bonpot as $bp)
      <tr>
          <td>{{ $bp->bpid }}</td>
          <td>{{ $bp->bonus }}</td>
          <td>{{ $bp->potongan }}</td>
          <td>
              <a href="{{ route('edit', $bp->bpid)}}" class="btn btn-warning btn-sm btn-flat" title="Edit"><i class="fa fa-edit"></i>Edit</a>
          </td>
      </tr>
      @empty
        <tr>
          <td>#</td>
            <td><input type="number" name="bonus" id="bonus" class="form-control" value="0"></td>
            <td><input type="number" name="potongan" id="potongan" class="form-control" value="0"></td>
            <td><button id="btnSimpan" class="btn btn-info btn-sm">Simpan</button></td>
        </tr>
      @endforelse
  </tbody>
</table>
<script>
  $('#btnSimpan').on('click', function(){
    console.log('submited');
    $.ajax({
      url: "{{route('submitBonpot')}}",
      type: 'POST',
      data: {
        "_token": "{{ csrf_token() }}",
        'tahun': $('#tahun').val(),
        'bulan': $('#bulan').val(),
        'bonus': $('#bonus').val(),
        'potongan': $('#potongan').val(),
      },
      success: function(data){
        Swal.fire({
          text: 'Berhasil!',
          title: 'Berhasil!',
          icon: 'success',
        });

        $('#dataWrapper').html(data);
      }
    });
  });
  $('#databonpot').dataTable({
    "columnDefs": [ {
      "targets": [0, 1, 2, 3],
      "orderable": false,
    } ]
  });
</script>