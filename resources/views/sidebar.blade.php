<div class="sidebar sidebar-style-2">
			<div class="sidebar-background"></div>
			<div class="sidebar-wrapper scrollbar scrollbar-inner">
				<div class="sidebar-content">
				@if(Auth::user()->level == 'admin')
				<!-- admin -->
					<ul class="nav nav-info">
						<li class="nav-item active">
							<a href="#">
								<i class="fas fa-home"></i>
								<p>Admin : {{Auth::user()->name}}</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('admin') }}">
								<p>Home</p>
							</a>
						</li>
						<!-- </li><li class="nav-item">
							<a href="{{ route('supplier.index') }}">
								<p>Supplier</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('supplybarang.index') }}">
								<p>Supply Barang</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('kerusakan.index') }}">
								<p>Kerusakan</p>
							</a>
						</li> -->
						<!-- <li class="nav-item">
							<a href="#">
								<p>Kelola Menu</p>
							</a>
						</li> -->
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Kelola Karyawan
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
								<a class="dropdown-item" href="{{ route('adminkar') }}">Data Karyawan</a>
								<a class="dropdown-item" href="{{ route('absen') }}">Absensi</a>
								<a class="dropdown-item" href="{{ route('bagian.index') }}">Bagian</a>
							</div>
						</li>
						<li class="nav-item">
							<a href="{{ route('akun') }}">
								<p>Kelola Akun</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('gaji.index') }}">
								<p>Kelola Gaji</p>
							</a>
						</li>
				
					</ul>
					@elseif(Auth::user()->level == 'pemilik')
					<!-- pemilik -->
					<ul class="nav nav-info">
						<li class="nav-item active">
							<a href="#">
								<i class="fas fa-home"></i>
								<p>Pemilik : {{Auth::user()->name}}</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('pemilik') }}">
								<p>Home</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('grafik/kinerja').'/'.date('Y') }}">
								<p>Kinerja Karyawan</p>
							</a>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Gaji
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
								<a class="dropdown-item" href="{{ route('Bonpot') }}">Kelola Bonus Potongan</a>
								<a class="dropdown-item" href="{{ route('gaji.index') }}">Laporan Gaji</a>
							</div>
						</li>
						<!-- <li class="nav-item nav-dropdown">
							<a class="nav-link nav-dropdown-toggle" href="#">Laporan</a>
							<ul class="nav-dropdown-items">
								<li class="nav-item"><a class="nav-link" href="#">Laporan Gaji</a></li>
								<li class="nav-item"><a class="nav-link" href="#">Laporan Pemasukan</a></li>
								<li class="nav-item"><a class="nav-link" href="#">Laporan Pengeluaran</a></li>
								<li class="nav-item"><a class="nav-link" href="#">Grafik</a></li>
							</ul>
						</li> -->
					</ul>
					@elseif(Auth::user()->level == 'kasir')
					<!-- kasir -->
					<ul class="nav nav-info">
						<li class="nav-item active">
							<a href="#">
								<i class="fas fa-home"></i>
								<p>Kasir : {{Auth::user()->name}}</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('admin') }}">
								<p>Home</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="components/alerts.html">
								<p>Transaksi Penjualan</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="components/alerts.html">
								<p>Rekap</p>
							</a>
						</li>
					</ul>
					@else
					<!-- karyawan -->
					<ul class="nav nav-info">
						<li class="nav-item active">
							<a href="#">
								<i class="fas fa-home"></i>
								<p>Karyawan : {{Auth::user()->name}}</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('karyawan') }}">
								<p>Home</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('absensi.index') }}">
								<p>Absensi</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('gaji.index') }}">
								<p>Gaji</p>
							</a>
						</li>
					</ul>
					@endif
				</div>
			</div>
		</div>