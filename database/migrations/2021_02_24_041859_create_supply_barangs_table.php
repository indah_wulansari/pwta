<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplyBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supply_barangs', function (Blueprint $table) {
            $table->string('bkode');
            $table->string('bnama');
            $table->string('btgl');
            $table->string('bjumlah');
            $table->string('hargabeli');
            $table->string('supkode');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supply_barangs');
    }
}
