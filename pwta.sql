/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 10.4.17-MariaDB : Database - pwta
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`pwta` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `pwta`;

/*Table structure for table `absensis` */

DROP TABLE IF EXISTS `absensis`;

CREATE TABLE `absensis` (
  `abid` int(11) NOT NULL,
  `karnik` int(11) NOT NULL,
  `karnama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abhar` date NOT NULL,
  `abmas` time NOT NULL,
  `abkel` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`abid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `absensis` */

/*Table structure for table `bagians` */

DROP TABLE IF EXISTS `bagians`;

CREATE TABLE `bagians` (
  `bagid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `bagnama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bagpok` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`bagid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `bagians` */

insert  into `bagians`(`bagid`,`bagnama`,`bagpok`,`created_at`,`updated_at`) values (1,'Admin',1500000,'2021-02-04 05:58:16','2021-02-04 16:35:51'),(4,'Kasir',1500000,'2021-02-04 16:37:27','2021-02-04 16:37:27'),(5,'Pemilik',2000000,'2021-02-04 16:37:47','2021-02-08 07:38:49');

/*Table structure for table `cabangs` */

DROP TABLE IF EXISTS `cabangs`;

CREATE TABLE `cabangs` (
  `cabkode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cabnama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `cabangs` */

insert  into `cabangs`(`cabkode`,`cabnama`,`created_at`,`updated_at`) values ('CAB-001','cabang1',NULL,NULL);

/*Table structure for table `failed_jobs` */

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `failed_jobs` */

/*Table structure for table `karyawans` */

DROP TABLE IF EXISTS `karyawans`;

CREATE TABLE `karyawans` (
  `karnik` bigint(20) unsigned NOT NULL,
  `karnama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `karjk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kartl` date NOT NULL,
  `karal` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kartelp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bagid` int(11) NOT NULL,
  `kartgljoin` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`karnik`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `karyawans` */

insert  into `karyawans`(`karnik`,`karnama`,`karjk`,`kartl`,`karal`,`kartelp`,`bagid`,`kartgljoin`,`created_at`,`updated_at`) values (12345678,'fulana','p','2021-02-08','bujel','12345678',4,'2021-02-08','2021-02-08 14:36:50','2021-02-09 15:30:43'),(987654321,'fulana','laki','2021-03-09','bujel','12345678',4,'2021-03-09','2021-03-09 13:03:20','2021-03-09 13:03:20'),(12345678987,'indah w','perempuan','1999-08-09','bujel','098765432123',5,'2021-02-04',NULL,'2021-02-09 15:30:51'),(56743245678,'indah','laki','2021-02-09','bujel kediri','12345678',1,'2021-02-09','2021-02-09 05:12:59','2021-02-09 05:12:59');

/*Table structure for table `kerusakans` */

DROP TABLE IF EXISTS `kerusakans`;

CREATE TABLE `kerusakans` (
  `rusakid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cabkode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rusaknama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rusaktgl` date NOT NULL,
  `rusakjml` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hargapenggantian` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `kerusakans` */

insert  into `kerusakans`(`rusakid`,`cabkode`,`rusaknama`,`rusaktgl`,`rusakjml`,`keterangan`,`hargapenggantian`,`created_at`,`updated_at`) values ('R-001','CAB-001','Lemari','2021-02-02','3','pintu copot','150000',NULL,NULL),('R-002','CAB-001','pintu 2','2021-02-03','12','bolong','3000001','2021-02-24 07:08:23','2021-02-24 07:19:33');

/*Table structure for table `menu_makanans` */

DROP TABLE IF EXISTS `menu_makanans`;

CREATE TABLE `menu_makanans` (
  `menukd` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bkode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stok` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hargajual` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`menukd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `menu_makanans` */

insert  into `menu_makanans`(`menukd`,`bkode`,`stok`,`hargajual`,`created_at`,`updated_at`) values ('M-002','B-0001','201','35001','2021-03-15 13:30:17','2021-03-15 13:52:28');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(19,'2014_10_12_200000_add_two_factor_columns_to_users_table',2),(21,'2021_01_29_162437_create_sessions_table',2),(24,'2019_12_14_000001_create_personal_access_tokens_table',3),(26,'2021_02_01_160201_create_bagians_table',4),(27,'2021_02_04_054703_create_menumakanans_table',5),(28,'2021_02_04_070552_create_karyawans_table',6),(29,'2021_02_15_122514_create_suppliers_table',7),(30,'2021_02_15_133240_create_pengeluarans_table',8),(31,'2021_02_16_130024_create_pengeluarans_table',9),(32,'2021_02_24_041859_create_supply_barangs_table',10),(33,'2021_02_24_061816_create_cabangs_table',11),(34,'2021_02_24_062543_create_kerusakans_table',12),(35,'2021_03_15_120411_create_menu_makanans_table',13),(36,'2021_04_03_145125_create_absensis_table',14);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `personal_access_tokens` */

DROP TABLE IF EXISTS `personal_access_tokens`;

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `personal_access_tokens` */

/*Table structure for table `sessions` */

DROP TABLE IF EXISTS `sessions`;

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sessions_user_id_index` (`user_id`),
  KEY `sessions_last_activity_index` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `sessions` */

insert  into `sessions`(`id`,`user_id`,`ip_address`,`user_agent`,`payload`,`last_activity`) values ('dnswLGK8iuCTdiiSsWSIHcuolRd41fFrICFU2CiH',1,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36','YTo1OntzOjk6Il9wcmV2aW91cyI7YToxOntzOjM6InVybCI7czozMDoiaHR0cDovL2xvY2FsaG9zdDo4MDAwL2FkbWlua2FyIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo2OiJfdG9rZW4iO3M6NDA6Ijl2ZzhSUHN0ZXFBb1ZJVWxxUlhHY05IQVNRU205dUtidnl0ZmJXb0IiO3M6NTA6ImxvZ2luX3dlYl81OWJhMzZhZGRjMmIyZjk0MDE1ODBmMDE0YzdmNThlYTRlMzA5ODlkIjtpOjE7czoxNzoicGFzc3dvcmRfaGFzaF93ZWIiO3M6NjA6IiQyeSQxMCRRSUhMVmxSdW9UTnpMYUxTRXc2MXJ1VDYyaUlUTm9vcEpBZllzdVc0eXlUNVU2ODdlMVE3dSI7fQ==',1617464426),('jLGbPqBV3QBQdHQj2MSWWFAv6MuSI3ITa7AQRKCy',3,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36','YTo1OntzOjk6Il9wcmV2aW91cyI7YToxOntzOjM6InVybCI7czoyOToiaHR0cDovL2xvY2FsaG9zdDo4MDAwL2Fic2Vuc2kiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjY6Il90b2tlbiI7czo0MDoiUFNvOURyOXBzT21ub3V1RkRmUzNlOHdpN2N2VXl6TmFwaUtHWjl0ZCI7czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6MztzOjE3OiJwYXNzd29yZF9oYXNoX3dlYiI7czo2MDoiJDJ5JDEwJHVyZDA1dVFseml2ck1TRnEwZWZ6MU9SeFN5c1htSjZxam5pYnZhNnlFSnprcFRudVRNZzNDIjt9',1617375438);

/*Table structure for table `suppliers` */

DROP TABLE IF EXISTS `suppliers`;

CREATE TABLE `suppliers` (
  `supkode` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `supnama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`supkode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `suppliers` */

insert  into `suppliers`(`supkode`,`supnama`,`created_at`,`updated_at`) values ('SUP-001','Riska N',NULL,'2021-02-15 13:18:00');

/*Table structure for table `supply_barangs` */

DROP TABLE IF EXISTS `supply_barangs`;

CREATE TABLE `supply_barangs` (
  `bkode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bnama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `btgl` date NOT NULL,
  `bjumlah` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hargabeli` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `supkode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`bkode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `supply_barangs` */

insert  into `supply_barangs`(`bkode`,`bnama`,`btgl`,`bjumlah`,`hargabeli`,`supkode`,`created_at`,`updated_at`) values ('B-0001','NUTRISARI','2021-02-10','3','20000','SUP-001',NULL,NULL),('B-002','Nutrisari jeruk','2020-05-24','2 lusin','200001','SUP-001','2021-02-24 05:44:27','2021-02-24 06:08:37'),('B-003','KOPI','2021-02-03','2 lusin','200000','SUP-001','2021-02-24 07:14:47','2021-02-24 07:14:47');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`username`,`email`,`email_verified_at`,`password`,`level`,`remember_token`,`created_at`,`updated_at`) values (1,'indah sindi','admin','admin@admin.com',NULL,'$2y$10$QIHLVlRuoTNzLaLSEw61ruT62iITNoopJAfYsuW4yyT5U687e1Q7u','admin',NULL,'2021-01-30 14:16:05','2021-01-30 14:16:05'),(2,'piaway','pemilik','pemilik@pemilik.com',NULL,'$2y$10$jo3hXWJX1tlpwPzzeN5XGOeCu5LBdiufuMBgrQCvkuOmANpumUKw.','pemilik',NULL,'2021-01-30 14:16:05','2021-01-30 14:16:05'),(3,'eko','karyawan','karyawan@karyawan.com',NULL,'$2y$10$urd05uQlzivrMSFq0efz1ORxSysXmJ6qjnibva6yEJzkpTnuTMg3C','karyawan',NULL,'2021-01-30 14:16:05','2021-01-30 14:16:05'),(4,'oke','kasir','kasir@kasir.com',NULL,'$2y$10$7S5TAFSFORIYp90X97ryluat3bOMEOZVKVTkN5lcMGHcE6Y7CKugy','kasir',NULL,'2021-01-30 14:16:05','2021-01-30 14:16:05'),(5,'indah','indah','indah@indah.com',NULL,'$2y$10$KTTJV.vYsuhQJzXjJDzamOcr/ANc0xR71.DPhSYmqjX2f1wj8GVuq','karyawan',NULL,'2021-02-02 16:37:34','2021-02-02 16:37:34'),(7,'Bagus','bagus','bagus@bagus.com',NULL,'$2y$10$He3rKSXBM30uh7oC2P4nn.Kbdi2OhZg.ltjttl7ufF9SoE7KAuOJi','kasir',NULL,'2021-02-02 16:40:14','2021-02-02 16:40:14'),(8,'gun','gun','gun@gun.com',NULL,'$2y$10$ValfS8JbolfXOfnecgUCs.oA32xcJFdUwFFBHw/65Jez52gwpp3QO','karyawan',NULL,'2021-02-03 12:39:54','2021-02-03 12:39:54'),(9,'Rifqi','rifqi','rifqi@rifqi.com',NULL,'$2y$10$LpxX/sLYTGnGIh4igB.W6uZX3Tx9dR5CNjmsYI/NRI2AlyOw7no1G','karyawan',NULL,'2021-02-03 12:47:05','2021-02-03 12:47:05'),(10,'aku','aku','aku@aku.com',NULL,'$2y$10$pgXPNj6aVQEgxG8YWZE6FuTKloJB0uSJWK0KH9CJ1ko6uuODqUwAe','kasir',NULL,'2021-02-03 12:50:10','2021-02-03 12:50:10'),(12,'saya','saya','saya@saya.com',NULL,'$2y$10$xCip1dVrWG6R2w/f1e8LB.hOdmR9kVXbbbHuVyVpOo22B7640cU/i','kasir',NULL,'2021-02-03 13:00:52','2021-02-03 13:00:52'),(13,'abdi','abdi','abdi@abdi.com',NULL,'$2y$10$KPK47LlinlGn2uwCaE0Ln.yF5E18WAUJnlPzE5ycQR0rxYpM5DOgC','karyawan',NULL,'2021-02-03 13:02:54','2021-02-03 13:02:54');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
