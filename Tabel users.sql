/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 10.4.17-MariaDB : Database - pwta
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`pwta` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `pwta`;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `karnik` bigint(20) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `karnik` (`karnik`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`karnik`) REFERENCES `karyawans` (`karnik`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`karnik`,`name`,`username`,`password`,`level`,`remember_token`,`created_at`,`updated_at`) values (20,3506052811010001,'Muchtar Al Husain','husain','$2y$10$6Tp.0uTSxqsVBQ2dslkK1er/DVUuPwDjtGl1QnAzcf.ESynfj3VZW','karyawan',NULL,'2021-05-28 21:27:06','2021-05-28 21:27:06'),(22,3506052501980005,'Andri Rio Pramudian','admin','$2y$10$QIHLVlRuoTNzLaLSEw61ruT62iITNoopJAfYsuW4yyT5U687e1Q7u','admin',NULL,NULL,NULL),(24,3506052505970006,'Bambang Sugianto','bambang','$2y$10$kbb8HOM22.A22i1Qs2Kx2etej6967q0b9ByGPBziohIpqFUIODyM6','karyawan',NULL,'2021-05-28 21:38:08','2021-05-28 21:38:08'),(25,3506055402000005,'Cindy Widyan Valentina','cindy','$2y$10$3GiqmQINLwyJ/uD8cauoV.5Ela0Ool1rHm7q5Q/NQuMcSi75/xHFK','karyawan',NULL,'2021-05-28 21:39:10','2021-05-28 21:39:10'),(26,3506265806910002,'Bella Lizawana','bella','$2y$10$1KW2TW5JhFk/MzH2imBmTuWsuS.Zxqr5muvEV2dXPqTJ9mULeLG1m','karyawan',NULL,'2021-05-28 21:51:36','2021-05-28 21:51:36'),(27,3506262909960002,'Danang Ragil Pamungkas','danang','$2y$10$o1afUbxsLbw7XPyNsFFFM.UuR8k.TmtXpbtKo/u.tR0QtV9D0NZdC','karyawan',NULL,'2021-05-28 21:52:00','2021-05-28 21:52:00'),(32,3505021406980001,'Karisma Piawai Sampurna','piawai','$2y$10$7J3wmZCVQzWQOUM9jqQhz.F0iCrmZSqUuCggfpPq6dXgh8jElnyBG','pemilik',NULL,'2021-06-13 20:24:15','2021-06-13 20:24:15');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
