<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layout');
});

Route::get('get/ajax/bonpot', 'App\Http\Controllers\PemilikController@getBonpot')->name('getBonpot');
// LOGIN
Route::get('login','App\Http\Controllers\AuthController@index')->name('login');
Route::post('proses_login','App\Http\Controllers\AuthController@proses_login')->name('proses_login');

Route::get('logout', 'App\Http\Controllers\AuthController@logout')->name('logout');

Route::group(['middleware' => ['auth']], function () {
    Route::group(['middleware' => ['cek_login:admin']], function () {
        Route::get('admin','App\Http\Controllers\AdminController@index')->name('admin');
        Route::get('absen','App\Http\Controllers\AdminController@show')->name('absen');
        Route::get('adminkar', 'App\Http\Controllers\KaryawanController@adminkar')->name('adminkar');

    }); 
    Route::group(['middleware' => ['cek_login:pemilik']], function () {
        Route::get('pemilik','App\Http\Controllers\PemilikController@index')->name('pemilik');
        Route::get('grafik/kinerja/{tahun}','App\Http\Controllers\PemilikController@Grafik')->name('Grafik');
        Route::get('Get_Grafik','App\Http\Controllers\PemilikController@Get_Grafik')->name('Get_Grafik');
        Route::get('bonpot','App\Http\Controllers\PemilikController@Bonpot')->name('Bonpot');
        Route::post('submit/bonpot', 'App\Http\Controllers\PemilikController@submitBonpot')->name('submitBonpot');
        Route::get('edit/{bpid}','App\Http\Controllers\PemilikController@edit')->name('edit');
        Route::any('update/{bpid}','App\Http\Controllers\PemilikController@update')->name('update');


    }); 
    Route::group(['middleware' => ['cek_login:karyawan']], function () {
        Route::get('/karyawan', 'App\Http\Controllers\KaryawanController@Dashkar')->name('karyawan');
    }); 
    Route::group(['middleware' => ['cek_login:kasir']], function () {
        Route::get('kasir','App\Http\Controllers\KasirController@index')->name('kasir');
    });   
});

Route::get('cetak/', 'App\Http\Controllers\KaryawanController@cetak')->name('cetak');
Route::resource('datakar', 'App\Http\Controllers\KaryawanController');



Route::get('print', 'App\Http\Controllers\GajiController@Print')->name('Print');

//REGISTER
Route::get('register', 'App\Http\Controllers\RegisterController@create')->name('register');
Route::post('register', 'App\Http\Controllers\RegisterController@store')->name('register');
Route::get('select2Karyawan', 'App\Http\Controllers\RegisterController@select2Karyawan')->name('select2Karyawan');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

//BAGIAN
Route::resource('bagian', 'App\Http\Controllers\BagianController');
// Route::get('bagian/{bagid}/konfirmasi',[BagianController::class], 'konfirmasi');

//  AKUN
Route::get('akun', 'App\Http\Controllers\AkunController@index')->name('akun');

//SUPPLIER
Route::resource('supplier', 'App\Http\Controllers\SupplierController');

//SUPPLYBARANG
Route::resource('supplybarang', 'App\Http\Controllers\SupplyBarangController');

//KERUSAKAN
Route::resource('kerusakan', 'App\Http\Controllers\KerusakanController');

// //MENU
Route::resource('menumakanan', 'App\Http\Controllers\MenuMakananController');

// //ABSENSI
Route::resource('absensi', 'App\Http\Controllers\AbsensiController');
Route::get('pulang/{abid}', 'App\Http\Controllers\AbsensiController@pulang')->name('pulang/');


// //GAJI
Route::resource('gaji', 'App\Http\Controllers\GajiController');
Route::get('data_table/gaji', 'App\Http\Controllers\GajiController@dataTable_gaji')->name('DTGaji');
Route::get('getKelalaian/{karnik}/{tanggal}', 'App\Http\Controllers\GajiController@getKelalaian')->name('getKelalaian');