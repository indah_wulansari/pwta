SELECT a.*, IF(jumlah_masuk=jumlah_hari,'10000','0') AS bonus FROM
(SELECT karnik,COUNT(abhar) AS jumlah_masuk,DAY(LAST_DAY(abhar)) AS jumlah_hari  FROM absensis GROUP BY MONTH(abhar))a

SELECT a.*, SUM(potongan) FROM
(SELECT absensis.*, IF(abmas>'10:00:00',1000,0) AS potongan FROM absensis)a
GROUP BY karnik,MONTH(abhar)

SELECT ab.karnik,kr.karnama,bg.bagnama, ab.abhar, ab.potongan, IF(jumlah_masuk=jumlah_hari,'10000','0') AS bonus,bagpok, (bagpok + IF(jumlah_masuk=jumlah_hari,'10000','0') - ab.potongan) AS total FROM
(
SELECT a.karnik,COUNT(a.abhar) AS jumlah_masuk,DAY(LAST_DAY(a.abhar)) AS jumlah_hari, SUM(potongan) AS potongan, a.abhar FROM 
(SELECT absensis.*, IF(abmas>'10:00:00',1000,0) AS potongan FROM absensis)a
 GROUP BY MONTH(abhar)
)ab
LEFT JOIN karyawans AS kr ON kr.karnik = ab.karnik 
LEFT JOIN bagians AS bg ON bg.bagid = kr.bagid

SELECT a.*, SUM(potongan) FROM
(SELECT absensis.*, IF(abmas > '10:00:00',1000,0) AS potongan FROM absensis)a
GROUP BY karnik,MONTH(abhar)

SELECT MONTH(abhar) FROM absensis 

SELECT DAY(LAST_DAY(NOW()))

CREATE VIEW rekap_gaji AS
SELECT ab.karnik,kr.karnama,bg.bagnama, ab.abhar, ab.jumlah_hari, ab.jumlah_masuk, ab.potongan, IF(jumlah_masuk=jumlah_hari,'50000','0') AS bonus,bagpok, (ROUND(bagpok/jumlah_hari*jumlah_masuk) + IF(jumlah_masuk=jumlah_hari,'50000','0') - ab.potongan) AS total FROM
(
SELECT a.karnik,COUNT(a.abhar) AS jumlah_masuk,DAY(LAST_DAY(a.abhar)) AS jumlah_hari, SUM(potongan) AS potongan, a.abhar FROM 
(SELECT absensis.*, IF(abmas>'16:00:00',1000,0) AS potongan FROM absensis)a
 GROUP BY karnik, MONTH(abhar)
)ab
LEFT JOIN karyawans AS kr ON kr.karnik = ab.karnik 
LEFT JOIN bagians AS bg ON bg.bagid = kr.bagid

SELECT absensis.*, IF(abmas > '16:00:00',1000,0) AS potongan FROM absensis
WHERE YEAR(abhar) = 2021
AND MONTH(abhar) = 6
AND karnik = 3571010626750002
AND abmas > '16:00:00'